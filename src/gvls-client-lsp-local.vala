/* gvls-client-lsp.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;
using GLib;

public class GVls.ClientLspLocal : Object, GVls.ClientLsp {
  Container _request;
  GVls.ServerLsp lsp_server;
  IOStream stream;
  Jsonrpc.Client _rcp_client;
  public Jsonrpc.Client rcp_client { get { return _rcp_client; } }
  public Workspace workspace { get; internal set; }
  public bool initialized { get; internal set; }
  public bool connected { get; internal set; }
  public Container request { get { return _request; } }
  public Cancellable cancellable { get; set; }
  
  construct {
    _request = new GContainer ();
    var ostream = new MemoryOutputStream.resizable ();
    var istream = new MemoryInputStream ();
    stream = new SimpleIOStream (istream, ostream);
    lsp_server = new ServerLspStream (stream);
    _rcp_client = new Jsonrpc.Client (stream);
  }
}
