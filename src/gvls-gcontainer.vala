/* gvls-gcontainer.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gee;

public class GVls.GContainer : Gee.ArrayList<Object>, GLib.ListModel, GVls.Container
{
  private HashMap<string,Object> _map = new HashMap<string,Object> ();

  public new void add (Object object) {
    (this as ArrayList<Object>).add (object);
    if (object is Symbol)
      _map.@set ((object as Symbol).name, object);
    if (object is Hashable)
      _map.@set ((object as ContainerHashable).hash (), object);
  }
  public new void remove (Object object) {
    for (int i = 0; i < get_n_items (); i++) {
      Object ob = get_item (i);
      if (ob == object) {
        (this as Gee.ArrayList<Object>).remove (ob);
      }
      if (object is Symbol && ob is Symbol) {
        _map.unset ((object as Symbol).name);
        if ((ob as Symbol).name == (object as Symbol).name) {
          (this as Gee.ArrayList<Object>).remove (ob);
        }
      }
      if (object is ContainerHashable && ob is ContainerHashable) {
        _map.unset ((object as ContainerHashable).hash ());
        if ((ob as ContainerHashable).hash() == (object as ContainerHashable).hash ()) {
          (this as Gee.ArrayList<Object>).remove (ob);
        }
      }
    }
  }

  public Object? find (Object key) {
    if (key is ContainerKeyString)
      return _map.@get ((key as ContainerKey).get_string ());
    return @get (index_of (key));
  }

  public Gee.Collection<Object> get_items () {
    if (_map.size == this.size) {
      return _map.values;
    }
    return this;
  }

  public Object? get_item (uint position) {
    return @get ((int) position);
  }
  public Type get_item_type () { return typeof (Object); }
  public uint get_n_items () { return (uint) size; }
  public Object? get_object (uint position) { return get_item (position); }
}
