/* GVls-gserver.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vala;
using Config;
using GLib;

public class GVls.GServer : GLib.Object, ContainerHashable, Server
{
  private GVls.Container _servers = new GContainer () as GVls.Container;
  private GLib.File _file;
  private string _string;
  private GVls.Container _symbols = new GContainer () as GVls.Container;
  private Vala.SourceFile source = null;
  private GVls.Container _vapi_dirs = new GContainer () as GVls.Container;
  private weak GVls.Server _root_server = null;
  private bool keywords_update = true;
  private GVls.Container _keywords = null;

  public GVls.Server root_server {
    get { return _root_server; }
  }
  public GVls.Container servers { get { return _servers; } }
  public GLib.File  file { get { return _file; } }
  public string content {
    get {
      return _string;
    }
    set {
      _string = value;
      try {
        parse_string (_string);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
      }
    }
  }
  public GVls.Container symbols { get{ return _symbols; } }
  public GLib.File prefix_dir { get; set; }
  public GVls.Container vapi_dirs { get { return _vapi_dirs; } }

  construct {
    prefix_dir = GLib.File.new_for_path (VAPIDIR);
    _file = GLib.File.new_for_uri ("file:///@RootServer@/%s".printf (GLib.Uuid.string_random ()));
    _string = "";
  }


  public void add_to_root_server (Server rserver) throws GLib.Error {
    rserver.servers.add (this);
    _root_server = rserver;
  }
  public void parse (GLib.File file) throws GLib.Error {
    if (!file.query_exists ())
      throw new ServerError.NO_FILE_ERROR ("File doesn't exists: %s", file.get_path ());
    var context = new Vala.CodeContext ();
    CodeContext.push (context);
    Vala.SourceFileType stype = Vala.SourceFileType.NONE;
    if (file.get_uri ().has_suffix (".vala")) {
      stype = Vala.SourceFileType.PACKAGE;
    }
    if (file.get_uri ().has_suffix (".vapi")) {
      stype = Vala.SourceFileType.PACKAGE;
    }
    if (stype == Vala.SourceFileType.NONE) {
      throw new ServerError.NO_FILE_ERROR ("File should be a .vala or .vapi ones: %s", file.get_path ());
    }
    source = new Vala.SourceFile (context, stype, file.get_path ());
    _file = file;
    parse_content (source);
    if (root_server != null) {
      root_server.vapi_dirs.add (file.get_parent ());
    } else {
      vapi_dirs.add (file.get_parent ());
    }
  }

  public void parse_string (string str) throws GLib.Error {
    var context = new Vala.CodeContext ();
    CodeContext.push (context);
    source = new Vala.SourceFile (context, Vala.SourceFileType.SOURCE, "stdin", str);
    parse_content (source);
  }
  public Symbol? get_symbol (string name) {
    assert (file is GLib.File);
    var k = new GContainerKeyString ();
    k.key = name;
    var col = _symbols.find (k) as Symbol;
    if (col != null) {
      return col;
    }
    if (name.contains (".")) {
      var t = name.split (".");
      Container sm = _symbols;
      for (int i = 0; i < t.length; i++) {
        k.key = t[i];
        var tsym = sm.find (k) as Symbol;
        if (tsym == null)  {
          sm = null;
          break;
        }
        if (tsym.symbol_kind == SymbolKind.VARIABLE) {
          k.key = tsym.data_type;
          tsym = sm.find (k) as Symbol;
          if (tsym == null)  {
            sm = null;
            break;
          }
        }
        sm = tsym as Container;
      }
      if (sm != null) {
        return sm as Symbol;
      }
    }
    k.key = name;
    foreach (Object obj in _symbols as Gee.ArrayList<Object>) {
      if (!(obj is GSymbol)) {
        continue;
      }
      var sym = (obj as GSymbol).find (k) as Symbol;
      if (sym != null) {
        return sym;
      }
    }
    for (int i = 0; i < servers.get_n_items (); i++) {
      Server s = servers.get_item (i) as Server;
      if (s == null) continue;
      var sym = s.get_symbol (name);
      if (sym != null) return sym;
    }
    return null;
  }

  public Symbol? find_at (Location loc) throws GLib.Error {
    if (_file == null)
      throw new ServerError.NO_FILE_ERROR ("No file is currently parsed");
    for (int i = 0; i < symbols.get_n_items (); i++) {
      var s = symbols.get_item (i) as Symbol;
      if (s == null) continue;
      if (s.is_at (loc)) return s;
    }
    return null;
  }

  public Server create_pkg_server (GLib.File file) throws GLib.Error {
    var srv = new GServer ();
    srv.parse (file);
    return srv;
  }

  private void parse_content (Vala.SourceFile source) {
    var parser = new Vala.Parser ();
    parser.parse_file (source);
    (_symbols as GContainer).clear ();
    var rns = new GSymbol (source.context.root as Vala.Symbol, _file);
    _symbols.add (rns);
    foreach (Vala.CodeNode cn in source.get_nodes ()) {
      if (cn is Vala.Symbol) {
        var sc = new GSymbol (cn as Vala.Symbol, _file);
        _symbols.add (sc);
      }
    }
    if (root_server != null) {
      try {
        root_server.add_all_using_namespaces ();
      } catch (GLib.Error e) {
        message ("Error adding namespace at root server: %s", e.message);
      }
    } else {
      try {
        add_using_namespaces ();
      } catch (GLib.Error e) {
        message ("Error adding namespace: %s", e.message);
      }
    }
    CodeContext.pop ();
    keywords_update = true;
    parsed ();
  }

  public GLib.ListModel find_using_namespaces () {
    var list = new GLib.ListStore (typeof (StringObject));
    if (source == null) {
      return list;
    }
    foreach (Vala.UsingDirective d in source.current_using_directives) {
      var s = new StringObject ();
      if (d.namespace_symbol != null) {
        if (d.namespace_symbol.name != null) {
          s.val = d.namespace_symbol.name;
          list.append (s);
        }
      }
    }
    return list;
  }
  public GVls.Container keywords {
    owned get {
      if (keywords_update) {
        // Scan the document to find keywords
        GScanner sc;
        Container list = null;
        if ("file:///@RootServer@/" in file.get_uri ()) {
          sc = new GVls.GScanner.from_string (content);
        } else {
          sc = new GVls.GScanner.from_file (file);
        }
        try {
          list = sc.start (this);
        } catch (GLib.Error e) {
          warning ("Error: %s", e.message);
        }
        _keywords = list;
        keywords_update = false;
      }
      return _keywords;
    }
  }
  // Hashable Interface
  public string hash () {
    if (file != null) {
      return file.get_uri ();
    }
    return "";
  }
  public bool equal (Object obj) {
    if (!(obj is Server)) {
      return false;
    }
    return (obj as Server).hash () == hash ();
  }
}

