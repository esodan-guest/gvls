/* gvls-client.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vala;

public interface GVls.Client : GLib.Object
{
  public abstract string str { get; internal set; }
  public abstract bool parse_mode { get; internal set; }
  public abstract Server server { get; construct set; }

  public signal void completion (GLib.ListModel list);

  public virtual SymbolKind symbol_kind (string str) {
    Symbol sym = server.get_symbol (str);
    if (sym == null) {
      return SymbolKind.UNKNOWN;
    }
    return sym.symbol_kind;
  }
  public virtual void push_text (string s) throws GLib.Error {
    if (server == null) {
      return;
    }
    if (str == null) {
      str = "";
    }
    string tstr = s;
    if (tstr.contains ("\n")) {
      if (tstr.replace ("\n", "").length == 0) {
        parse_mode = false;
      } else {
        parse_mode = true;
      }
    }
    str += tstr;
    if (tstr.has_suffix (".")) {
      tstr = tstr.strip ();
      tstr = tstr.substring (0, tstr.length -1);
      var sym = server.get_symbol (tstr);
      if (sym != null) {
        completion (sym as GLib.ListModel);
      }
    }
    if (parse_mode) {
      server.parse_string (str);
    }
  }
}

