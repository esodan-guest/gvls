/* gvls-document-highlights.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.HighLight : Object {
  public signal void text_highligths_request (GVls.RequestMessage msg);

  public virtual void report_text_highligths (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/documentHighlight";
    msg.@params.add (@params);
    text_highligths_request (msg);
  }
}

/**
 * A document highlight is a range inside a text document which deserves
 * special attention. Usually a document highlight is visualized by changing
 * the background color of its range.
 *
 */
public interface GVls.DocumentHighlight : Object {
  /**
   * The range this highlight applies to.
   */
  public abstract Range range { get; set; }

  /**
   * The highlight kind, default is DocumentHighlightKind.Text.
   */
  public abstract DocumentHighlightKind kind { get; set; }
}

/**
 * A document highlight kind.
 */
public enum GVls.DocumentHighlightKind {
  UNKNOWN,
  /**
   * A textual occurrence.
   */
  TEXT,

  /**
   * Read-access of a symbol, like reading a variable.
   */
  READ,

  /**
   * Write-access of a symbol, like writing to a variable.
   */
  WRITE;

  public string to_string () {
    string str = "";
    switch (this) {
      case TEXT:
        str = "Text";
        break;
      case READ:
        str = "Read";
        break;
      case WRITE:
        str = "Write";
        break;
    }
    return str;
  }
}

public interface GVls.DocumentSymbolParams : Object {
  /**
   * The text document.
   */
  public abstract TextDocumentIdentifier text_document { get; set; }
}
