/* gvls-gworkspace.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;
using GLib;

public class GVls.GWorkspace : Object, ContainerHashable, Workspace
{
  Container _folders = new GContainer ();
  Container _whatched_files = new GContainer ();
  public Container folders { get { return _folders; } }
  public Container whatched_files { get { return _whatched_files; } }
  
  public string hash () { return "@workspace@-" + GLib.Uuid.string_random (); }
  public bool equal (Object obj) {
    if (!(obj is Workspace)) return false;
    if ((obj as ContainerHashable).hash () == hash ()) return true;
    return false;
  }
}
