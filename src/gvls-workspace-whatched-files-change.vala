/* gvls-workspace-whatched-files-change.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.DidChangeWatchedFilesParams : Object {
  /**
   * The actual file events of type {@link FileEvent}
   */
  public abstract Container changes { get; }
}

/**
 * An event describing a file change.
 */
public interface GVls.FileEvent : Object, GVls.ContainerHashable {
  public abstract GLib.File file { get; set; }
  /**
   * The file's URI.
   */
  public string uri { owned get { return file.get_uri (); } }
  /**
   * The change type.
   */
  public abstract FileChangeType change_type { get; set; }
}

/**
 * The file event type.
 */
public enum GVls.FileChangeType {
  UNKNOWN,
  /**
   * The file got created.
   */
  CREATED,
  /**
   * The file got changed.
   */
  CHANGED,
  /**
   * The file got deleted.
   */
  DELETED;

  public string to_string () {
    string str = "";
    switch (this) {
      case CREATED:
        str = "Created";
        break;
      case CHANGED:
        str = "Changed";
        break;
      case DELETED:
        str = "Deleted";
        break;
      default:
       str = "Unknown";
       break;
    }
    return str;
  }
}


/**
 * Describe options to be used when registering for text document change events.
 */
public interface GVls.DidChangeWatchedFilesRegistrationOptions : Object {
  /**
   * The watchers to register of the type {@link FileSystemWatcher}.
   */
  public abstract Container watchers  { get; }
}

public interface FileSystemWatcher : Object {
  /**
   * The  glob pattern to watch
   */
  public abstract string globPattern { get; set; }

  /**
   * The kind of events of interest. If omitted it defaults
   * to WatchKind.Create | WatchKind.Change | WatchKind.Delete
   * which is 7.
   */
  public abstract WatchKind kind { get; set; }
}
/**
 * File system watch kind
 */
[Flags]
public enum WatchKind {
  UNKNOWN,
  /**
   * Interested in create events.
   */
  CREATE,

  /**
   * Interested in change events
   */
  CHANGE,

  /**
   * Interested in delete events
   */
  DELETE;

  public string to_string () {
    string str = "";
    var a = new Gee.ArrayList<string> ();
    if (CREATE in this) {
      a.add ("Created");
    }
    if (CHANGE in this) {
      a.add ("Changed");
    }
    if (DELETE in this) {
      a.add ("Deleted");
    }
    if (a.size == 0) {
      return "Unknown";
    }
    for (int i = 0; i < a.size; i++) {
      str += a.get (i);
      if (i+1 < a.size) {
        str += "|";
      }
    }
    return str;
  }
}


