/* GVls-gserver.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 public class GVls.GDocumentSymbol : Object, ListModel, Container, DocumentSymbol, Symbol {
  private Symbol _symbol;
  private Location _location;
  
  public GDocumentSymbol (Symbol sym, Location loc) {
    _symbol = sym;
    _location = loc;
  }
  // DocumentSymbol
  public string name { get { return _symbol.name; } }
  public string? detail { get { return _symbol.detail; } }
  public SymbolKind kind { get { return _symbol.kind; } }
  public bool deprecated { get { return _symbol.deprecated; } }
  public Range range { get { return _location as Range; } }
  public Range selection_range { get { return _location as Range; } }
  public Container children { get { return _symbol.children; } }
  // Container
  public void add (Object object) { _symbol.add (object); }
  public void remove (Object object) { _symbol.remove (object); }
  public Object? find (Object key) { return _symbol.find (key); }

  // ListModel implementation
  public Object? get_item (uint position) { return _symbol.get_item (position); }
  public Type get_item_type () { return typeof (Symbol); }
  public uint get_n_items () { return _symbol.get_n_items (); }
  public Object? get_object (uint position) { return get_item (position); }
  // Symbol
  public Location location { get { return _location; } }
  public Package package { get { return _symbol.package; } }
  public Documentation documentation { get { return _symbol.documentation; } }
  public SymbolKind symbol_kind { get { return _symbol.symbol_kind; } }
  public string full_name { owned get { return _symbol.full_name; } }
  public string data_type { get { return _symbol.data_type; } }
  public Symbol? get_child (string name) { return _symbol.get_child (name); }
  public void add_child (Symbol sym)  { _symbol.add_child (sym); }
}
