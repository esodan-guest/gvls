/* gvls-gsymbol.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using Vala;
using Gee;
using GLib;

public class GVls.GSymbol : Gee.AbstractMap<string,GVls.Symbol>,
                             GLib.ListModel,
                             GVls.Container,
                             GVls.DocumentSymbol,
                             GVls.Symbol
{
  private Location _location;
  private Package _package;
  private Documentation _documentation;
  private Vala.Symbol _symbol;
  private string _data_type = "@UNKNOWN@";
  private GLib.File _file;

  public string name {
    get {
      if (_symbol is Vala.Namespace && _symbol.name == null) {
        return "@RootNamespace@";
      }
      return _symbol.name;
    }
  }
  public string full_name {
    owned get {
      // FIXME: See Vala #690
      return _symbol.get_full_name ();
    }
  }
  public Location location { get { return _location; } }
  public Package package { get { return _package; } }
  public Documentation documentation { get { return _documentation; } }

  public string data_type {
    get  {
      _data_type = name;
      switch (symbol_kind) {
        case SymbolKind.VARIABLE:
          _data_type = (_symbol as Vala.Variable).variable_type.to_string();
          break;
        default:
          break;
      }
      return _data_type;
    }
  }
  public SymbolKind symbol_kind {
    get {
      if (_symbol is Namespace) {
        return SymbolKind.NAMESPACE;
      }
      if (_symbol is Class) {
        return SymbolKind.CLASS;
      }
      if (_symbol is Interface) {
        return SymbolKind.INTERFACE;
      }
      if (_symbol is Method) {
        return SymbolKind.METHOD;
      }
      if (_symbol is Variable) {
        return SymbolKind.VARIABLE;
      }
      if (_symbol is Property) {
        return SymbolKind.PROPERTY;
      }
      if (_symbol is ObjectType) {
        return SymbolKind.OBJECT;
      }
      if (_symbol is Enum) {
        return SymbolKind.ENUM;
      }
      if (_symbol is Vala.EnumValue) {
        return SymbolKind.ENUM_MEMBER;
      }
      if (_symbol is Struct) {
        return SymbolKind.STRUCT;
      }
      return SymbolKind.UNKNOWN;
    }
  }

  public GSymbol (Vala.Symbol sym, GLib.File file) {
    _symbol = sym;
    if (sym.source_reference != null) {
      _location = new GLocation.from_values (file,
                                            sym.source_reference.begin.line,
                                            sym.source_reference.begin.column,
                                            sym.source_reference.end.line,
                                            sym.source_reference.end.column);
    } else {
      _location = new GLocation ();
    }
    var ns = new Vala.Namespace ("<NoNamespace>");
    _package = new GPackage (ns, file);
    _documentation = new GDocumentation (sym);
    _file = file;
  }


  public Symbol? get_child (string name) {
    var k = new GContainerKeyString ();
    k.key = name;
    return find (k) as Symbol;
  }

  public void add_child (Symbol sym) {
    add (sym);
  }

  public Vala.Symbol get_symbol () { return _symbol; }

  // Container implementation

  /* Used for ListModel implementation*/
  private Gee.ArrayList<Symbol> _symbols = null;

  private Symbol? _current = null;
  private Entries _entries = null;

  public void add (Object object) {
      return;
  }
  public void remove (Object object) {
    return;
  }
  public Object? find (Object key) {
    if (key is ContainerKeyString) {
      var s = @get ((key as ContainerKey).get_string ());
      if (s != null) {
        return new GSymbol ((s as GSymbol).get_symbol (), _file) as Object;
      }
    }
    return null;
  }

  public void update_list () {
    _symbols = null;
    create_list ();
  }

  // ListModel implementation
  public Object? get_item (uint position) {
    create_list ();
    return _symbols.@get ((int) position);
  }
  public Type get_item_type () { return typeof (Symbol); }
  public uint get_n_items () {
    create_list ();
    return (uint) _symbols.size;
  }
  public Object? get_object (uint position) { return get_item (position); }

  // AbstractMap implementation
  public override int size { get { return _symbol.scope.get_symbol_table ().size; } }
  public override bool read_only { get { return true; } }
  public override Gee.Set<string> keys { owned get { return new Set (_symbol.scope.get_symbol_table ().get_keys (), _file); } }
  public override Gee.Collection<Symbol> values { owned get { return new Collection (_symbol, _file); } }
  public override Gee.Set<Gee.Map.Entry<string,Symbol>> entries {
    owned get {
      _entries = new Entries (_symbol, _file);
      return _entries;
    }
  }
  public override bool has_key (string key) {
    return _symbol.scope.get_symbol_table ().contains (key);
  }
  public override bool has (string key, Symbol value) {
    if (!has_key (key))
      return false;
    var v = @get (key);
    if (v == null)
      return false;
    return (v as GSymbol).get_symbol () == (value as GSymbol).get_symbol ();
  }
  public override Symbol get (string key) {
    if (_symbol.scope != null) {
      var v = _symbol.scope.lookup (key);
      if (v != null) {
        _current = new GSymbol (v, _file) as Symbol;
        return _current;
      } else {
        switch (symbol_kind) {
          case SymbolKind.CLASS:
          case SymbolKind.INTERFACE:
            // FIXME: Maybe using current location can help to get faster a symbol in a Block
            foreach (Vala.Method m in (_symbol as ObjectTypeSymbol).get_methods ()) {
              if (m.body != null) {
                var sr = block_find_symbol (m.body, key);
                if (sr != null) {
                  _current = new GSymbol (sr, _file) as Symbol;
                  return _current;
                }
              }
            }
            break;
        }
      }
    }
    _current = null;
    return _current;
  }
  public override void set (string key, Symbol value) {}
  public override bool unset (string key, out Symbol value) { return false; }
  public override Gee.MapIterator<string,Symbol> map_iterator () {
    return new MapIterator (_symbol, _file);
  }
  public override void clear () {}

  private void create_list () {
    if (_symbols == null) {
      _symbols = new Gee.ArrayList<Symbol> ();
      if (_symbol.scope == null)
        return;
      var symt = _symbol.scope.get_symbol_table ();
      if (symt == null)
        return;
      var vals = symt.get_values ();
      if (vals == null)
        return;
      foreach (Vala.Symbol s in vals)  {
        _symbols.add (new GSymbol (s, _file) as GVls.Symbol);
      }
    }
  }

  private Vala.Symbol? block_find_symbol (Vala.Block block, string key) {
    foreach (Vala.Statement st in block.get_statements ()) {
      if (st is Vala.DeclarationStatement) {
        var dc = (st as Vala.DeclarationStatement).declaration;
        if (dc.name == key) {
          return dc;
        }
      }
      if (st is Block) {
        var sr = block_find_symbol (st as Block, key);
        if (sr != null) {
          return sr;
        }
      }
    }
    return null;
  }

  private class MapIterator : GLib.Object, Gee.MapIterator<string,Symbol>
  {
    private Vala.MapIterator<string,Vala.Symbol> iter;
    private Symbol _current = null;
    private GLib.File _file;

    public MapIterator (Vala.Symbol sym, GLib.File file) {
      iter = sym.scope.get_symbol_table ().map_iterator ();
      _file = file;
    }

    public bool next () {
      return iter.next ();
    }
    public bool has_next () {
      return false;
    }
    public string get_key () {
      return iter.get_key ();
    }
    public Symbol get_value () {
      _current = new GSymbol (iter.get_value (), _file) as Symbol;
      return _current;
    }
    public void set_value (Symbol value) {}
    public void unset () {}
    public bool valid { get { return false; } }
    public bool mutable { get{ return false; }  }
    public bool read_only { get{ return true; }  }
  }

  private class Entries : Gee.AbstractCollection<Gee.Map.Entry<string,Symbol>>,
                          Gee.Set<Gee.Map.Entry<string,Symbol>>
  {
    private Vala.Symbol _symbol;
    private Vala.Map<string,Vala.Symbol> _col;
    private GLib.File _file;

    public Entries (Vala.Symbol sym, GLib.File file) {
      _symbol = sym;
      _col = sym.scope.get_symbol_table ();
      _file = file;
    }

    // Gee.Set implementation
    public new Gee.Set<Gee.Map.Entry<string,Symbol>> read_only_view { owned get { return this; } }

    public override int size { get { return _col.size; } }
    public override bool read_only { get { return true; } }
    public override bool contains (Gee.Map.Entry<string,Symbol> item) {
      if (!_col.contains (item.key)) {
        return false;
      }
      var i = _col.@get (item.key);
      if (i != (item.value as GSymbol).get_symbol ()) {
        return false;
      }
      return true;
    }
    public override bool add (Gee.Map.Entry<string,Symbol> item) { return false; }
    public override bool remove (Gee.Map.Entry<string,Symbol> item) { return false; }
    public override void clear () {}
    public override Gee.Iterator<Gee.Map.Entry<string,Symbol>> iterator () {
      return new EntriesIter (_symbol, _file) as Gee.Iterator<Gee.Map.Entry<string,Symbol>>;
    }
    private class EntriesIter : Gee.Map.Entry<string,Symbol>
    {
      private Symbol _current = null;
      public EntriesIter (Vala.Symbol sym, GLib.File file) {
        _current = new GSymbol (sym, file) as Symbol;
      }
      public override string key {
        get {
          return _current.name;
        }
      }
		  public override Symbol value {
		    get {
		      return _current;
		    }
		    set {}
		  }
		  public override bool read_only { get { return true; } }
    }
  }

  // Collection<Symbol> implementation
  private class Collection : Gee.AbstractCollection<Symbol> {
    private Vala.Symbol _symbol;
    protected Vala.Collection<Vala.Symbol> _vals;
    private GLib.File _file;

    public Collection (Vala.Symbol sym, GLib.File file) {
      _symbol = sym;
      _vals = _symbol.scope.get_symbol_table ().get_values ();
      _file = file;
    }
    // Gee.AbstractCollection implementation
    public override int size { get { return _vals.size; } }
    public override bool read_only { get { return true; } }
    public override bool contains (Symbol item) {
      return _vals.contains ((item as GSymbol).get_symbol ());
    }
    public override bool add (Symbol item) { return false; }
    public override bool remove (Symbol item) { return false; }
    public override void clear () {}
    public override Gee.Iterator<Symbol> iterator () { return new Iterator (_vals.iterator (), _file); }

    private class Iterator : Object, Gee.Traversable<Symbol>, Gee.Iterator<Symbol>
    {
      private GLib.File _file;
      private Vala.Iterator<Vala.Symbol> _iter;
      private Symbol? _current = null;

      public Iterator (Vala.Iterator<Vala.Symbol> iter, GLib.File file) {
        _iter = iter;
        _file = file;
      }

      public new bool foreach (ForallFunc<Symbol> f) {
        while (next ()) {
          var c = this.@get ();
          if (!f (c)) return false;
        }
        return true;
      }

      public bool next () { return _iter.next (); }
      public bool has_next () { return _iter.has_next (); }
      public bool valid { get { return false; } }
      public bool read_only { get { return true; } }
      public new Symbol @get () {
        var item = _iter.@get ();
        _current = new GSymbol (item, _file) as Symbol;
        return _current;
      }
      public void remove () {}
    }
  }
  private class Set : Gee.AbstractCollection<string>, Gee.Set<string>
  {
    protected Vala.Collection<string> _vals;
    private GLib.File _file;

    public Set (Vala.Collection<string> vals, GLib.File file) {
      _vals = vals;
      _file = file;
    }

    // Gee.Set implementation
    public new Gee.Set<string> read_only_view { owned get { return this; } }
    // Gee.AbstractCollection implementation
    public override int size { get { return _vals.size; } }
    public override bool read_only { get { return true; } }
    public override bool contains (string item) {
      return _vals.contains (item);
    }
    public override bool add (string item) { return false; }
    public override bool remove (string item) { return false; }
    public override void clear () {}
    public override Gee.Iterator<string> iterator () { return new Iterator (_vals.iterator (), _file); }

    private class Iterator : Object, Gee.Traversable<string>, Gee.Iterator<string>
    {
      private Vala.Iterator<string> _iter;
      private GLib.File _file;

      public Iterator (Vala.Iterator<string> iter, GLib.File file) {
        _iter = iter;
        _file = file;
      }

      public new bool foreach (ForallFunc<string> f) {
        while (next ()) {
          var c = this.@get ();
          if (!f (c)) return false;
        }
        return true;
      }

      public bool next () { return _iter.next (); }
      public bool has_next () { return _iter.has_next (); }
      public bool valid { get { return false; } }
      public bool read_only { get { return true; } }
      public new string get () {
        return _iter.get ();
      }
      public void remove () {}
    }
  }

  // DocumentSymbol implementation
  public string? detail { get { return null; } }
  public SymbolKind kind { get { return symbol_kind; } } // FIXME: duplicated
  public bool deprecated { get { return false; } }
  public Range range { get { return location as Range; } }
  public Range selection_range { get { return location as Range; } }
  public Container children { get { return this as Container; } }

}
