/* gvls-symbol.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Symbol : GLib.Object, GLib.ListModel, Container, DocumentSymbol
{
  public abstract Location location { get; }
  public abstract Package package { get; }
  public abstract Documentation documentation { get; }
  public abstract SymbolKind symbol_kind { get; }
  public abstract string full_name { owned get; }
  public abstract string data_type { get; }
  public abstract Symbol? get_child (string name);
  public abstract void add_child (Symbol sym);

  public virtual bool is_at (Location loc) {
    if (location.file.get_uri () != loc.file.get_uri ()) {
      return false;
    }
    if (location.start.line < 0 || location.end.line < 0 || location.end.character < 0 || location.end.line < 0) {
      return false;
    }
    if (loc.start.line < 0 || loc.end.line < 0 || loc.end.character < 0 || loc.end.line < 0) {
      return false;
    }
    if (loc.start.line != location.start.line) {
      return false;
    }
    if (loc.start.character < location.start.character
        || loc.end.character > location.end.character) {
      return false;
    }
    return true;
  }

}


public enum GVls.SymbolKind {
  UNKNOWN,
  FILE,
  MODULE,
  NAMESPACE,
  PACKAGE,
  CLASS,
  METHOD,
  PROPERTY,
  FIELD,
  CONSTRUCTOR,
  ENUM,
  INTERFACE,
  FUNCTION,
  VARIABLE,
  CONSTANT,
  STRING,
  NUMBER,
  BOOLEAN,
  ARRAY,
  OBJECT,
  KEY,
  NULL,
  ENUM_MEMBER,
  STRUCT,
  EVENT,
  OPERATOR,
  TYPE_PARAMETER;
  public string to_string () {
    string str = "Unknown";
    switch (this) {
      case UNKNOWN:
        break;
      case FILE:
        str = "File";
        break;
      case MODULE:
        str = "Module";
        break;
      case NAMESPACE:
        str = "Namespace";
        break;
      case PACKAGE:
        str = "Package";
        break;
      case CLASS:
        str = "Class";
        break;
      case METHOD:
        str = "Method";
        break;
      case PROPERTY:
        str = "Property";
        break;
      case FIELD:
        str = "Field";
        break;
      case CONSTRUCTOR:
        str = "Constructor";
        break;
      case ENUM:
        str = "Enum";
        break;
      case INTERFACE:
        str = "Interface";
        break;
      case FUNCTION:
        str = "Function";
        break;
      case VARIABLE:
        str = "Variable";
        break;
      case CONSTANT:
        str = "Constant";
        break;
      case STRING:
        str = "String";
        break;
      case NUMBER:
        str = "Number";
        break;
      case BOOLEAN:
        str = "Boolen";
        break;
      case ARRAY:
        str = "Array";
        break;
      case OBJECT:
        str = "Object";
        break;
      case KEY:
        str = "Key";
        break;
      case NULL:
        str = "Null";
        break;
      case ENUM_MEMBER:
        str = "EnumMember";
        break;
      case STRUCT:
        str = "Struct";
        break;
      case EVENT:
        str = "Event";
        break;
      case OPERATOR:
        str = "Operator";
        break;
      case TYPE_PARAMETER:
        str = "TypeParameter";
        break;
    }
    return str;
  }
}

public class GVls.SymbolKindObject : GVls.EnumObject {
  construct {
    enum_type = typeof (SymbolKind);
  }
  public SymbolKindObject (SymbolKind kind) {
    val = kind;
  }
  public SymbolKind get_enum () { return (SymbolKind) val; }
  public override string to_string () { return ((SymbolKind) val).to_string (); }
}
