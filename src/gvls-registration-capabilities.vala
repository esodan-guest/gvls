/* gvls-registration-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * General parameters to register for a capability.
 */
public interface GVls.Registration {
  /**
   * The id used to register the request. The id can be used to deregister
   * the request again.
   */
  [CCode (nick="id")]
  public abstract  string id { get; set; }

  /**
   * The method / capability to register for.
   */
  [CCode (nick="method")]
  public abstract  string method { get; set; }

  /**
   * Options necessary for the registration.
   */
  [CCode (nick="registerOptions")]
  public abstract  Object register_options { get; set; }
}

public interface GVls.RegistrationParams {
  /**
   * Registration capabilities.
   *
   * Container holds items of type {@link Registration}
   */
  [CCode (nick="registrations")]
  public abstract  Container registrations { get; set; }
}

