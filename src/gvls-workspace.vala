/* gvls-workspace.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Workspace : Object, GVls.ContainerHashable {
  public signal void did_change_workspace_folders (GVls.RequestMessage msg);
  public signal void configuration_request (GVls.RequestMessage msg);
  public signal void whatched_files_change (GVls.RequestMessage msg);
  public signal void requrested_symbol (GVls.RequestMessage msg);
  public signal void opened_text_document (GVls.RequestMessage msg);
  public signal void changed_text_document (GVls.RequestMessage msg);
  public signal void will_save_text_document (GVls.RequestMessage msg);
  public signal void will_save_wait_until_text_document (GVls.RequestMessage msg);
  public signal void saved_text_document (GVls.RequestMessage msg);
  public signal void closed_text_document (GVls.RequestMessage msg);
  public signal void file_added (GVls.FileEvent fe);
  public signal void file_deleted (GVls.FileEvent fe);
  public signal void file_changed (GVls.FileEvent fe);
  /**
   * List of {@link GLib.File} folders watched by this workspace.
   */
  public abstract Container folders { get; }
  /**
   * List of {@link GLib.File} files watched by this workspace.
   */
  public abstract Container whatched_files { get; }
  public virtual void report_change_folders (GVls.RequestMessage msg, DidChangeWorkspaceFoldersParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/didChangeWorkspaceFolders";
    msg.@params.add (@params as Object);
    for (int i = 0; i < @params.event.removed.get_n_items (); i++) {
      folders.remove (@params.event.removed.get_item (i));
    }
    for (int i = 0; i < @params.event.added.get_n_items (); i++) {
      folders.add (@params.event.added.get_item (i));
    }
    did_change_workspace_folders (msg);
  }
  public virtual void report_change_configuration (GVls.RequestMessage msg, ConfigurationParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/configuration";
    msg.@params.add (@params);
    configuration_request (msg);
  }
  public virtual void report_change_whatched_files (GVls.RequestMessage msg, DidChangeWatchedFilesParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/didChangeWatchedFiles";
    msg.@params.add (@params);
    for (int i = 0; i < @params.changes.get_n_items (); i++) {
      var change = @params.changes.get_item (i) as FileEvent;
      switch (change.change_type) {
        case FileChangeType.CREATED:
          whatched_files.add (change);
          file_added (change);
          break;
        case FileChangeType.CHANGED:
          file_changed (change);
          break;
        case FileChangeType.DELETED:
          whatched_files.remove (change);
          file_deleted (change);
          break;
      }
    }
    whatched_files_change (msg);
  }
  public virtual void report_request_symbol (GVls.RequestMessage msg, WorkspaceSymbolParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/symbol";
    msg.@params.add (@params);
    requrested_symbol (msg);
  }
  public virtual void report_open_text_document (GVls.RequestMessage msg, DidOpenTextDocumentParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/didOpen";
    msg.@params.add (@params);
    opened_text_document (msg);
  }
  public virtual void report_change_text_document (GVls.RequestMessage msg, DidChangeTextDocumentParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/didChange";
    msg.@params.add (@params);
    changed_text_document (msg);
  }
  public virtual void report_will_save_text_document (GVls.RequestMessage msg, WillSaveTextDocumentParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/willSave";
    msg.@params.add (@params);
    will_save_text_document (msg);
  }
  public virtual void report_will_save_wait_until_text_document (GVls.RequestMessage msg, WillSaveTextDocumentParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/willSave";
    msg.@params.add (@params);
    will_save_wait_until_text_document (msg);
  }
  public virtual void report_did_save_text_document (GVls.RequestMessage msg, DidSaveTextDocumentParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/didSave";
    msg.@params.add (@params);
    saved_text_document (msg);
  }
  public virtual void report_close_text_document (GVls.RequestMessage msg, DidCloseTextDocumentParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "workspace/didClose";
    msg.@params.add (@params);
    closed_text_document (msg);
  }
}
