/* gvls-workspace.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public interface GVls.CodeFormating : Object {
  public signal void document_formating_request (GVls.RequestMessage msg);
  public signal void range_formating_request (GVls.RequestMessage msg);
  public signal void on_type_formating_request (GVls.RequestMessage msg);

  public virtual void report_document_formating (GVls.RequestMessage msg, DocumentOnTypeFormattingParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/formatting";
    msg.@params.add (@params);
    document_formating_request (msg);
  }
  public virtual void report_range_formating (GVls.RequestMessage msg, DocumentOnTypeFormattingParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/rangeFormatting";
    msg.@params.add (@params);
    range_formating_request (msg);
  }
  public virtual void report_on_type_formating (GVls.RequestMessage msg, DocumentOnTypeFormattingParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/onTypeFormatting";
    msg.@params.add (@params);
    on_type_formating_request (msg);
  }

}


public interface GVls.DocumentFormattingParams : Object {
  /**
   * The document to format.
   */
  public abstract TextDocumentIdentifier text_document { get; set; }

  /**
   * The format options.
   */
  public abstract FormattingOptions options { get; set; }
}

/**
 * Value-object describing what options formatting should use.
 */
public interface GVls.FormattingOptions : Object {
  /**
   * Size of a tab in spaces.
   */
  public abstract int tab_size { get; set; }

  /**
   * Prefer spaces over tabs.
   */
  public abstract bool insert_spaces { get; set; }

  /**
   * Signature for further properties.
   */
  public abstract string key { get; set; }
}

public interface GVls.DocumentRangeFormattingParams : Object {
  /**
   * The document to format.
   */
  public abstract TextDocumentIdentifier text_document { get; set; }

  /**
   * The range to format
   */
  public abstract Range range { get; set; }

  /**
   * The format options
   */
  public abstract FormattingOptions options { get; set; }
}

public interface GVls.DocumentOnTypeFormattingParams : Object {
  /**
   * The document to format.
   */
  public abstract TextDocumentIdentifier textDocument { get; set; }

  /**
   * The position at which this request was sent.
   */
  public abstract Position position { get; set; }

  /**
   * The character that has been typed.
   */
  public abstract string ch { get; set; }

  /**
   * The format options.
   */
  public abstract FormattingOptions options { get; set; }
}

public interface GVls.DocumentOnTypeFormattingRegistrationOptions : Object, TextDocumentRegistrationOptions {
  /**
   * A character on which formatting should be triggered, like `}`.
   */
  public abstract string firstTriggerCharacter { get; set; }
  /**
   * More trigger characters.
   *
   * Container's items are of the type {@link StringObject}
   */
  public abstract Container more_trigger_character { get; }
}


