/* gvls-server.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Config;

public interface GVls.Server : GLib.Object, ContainerHashable
{
  /**
   *
   */
  public signal void parsed ();
  /**
   *
   */
  public signal void found_using_directive (string ns);
  /**
   *
   */
  public abstract GVls.Server root_server { get; }
  /**
   *
   */
  public abstract GVls.Container servers { get; }
  /**
   *
   */
  public abstract GLib.File  file { get; }
  /**
   * If the string to parse is not from a file, then use this
   * as a content buffer, parsing hapends any time you
   * set this property.
   */
  public abstract string content { get; set; }
  /**
   *
   */
  public abstract GVls.Container symbols { get; }
  /**
   *
   */
  public abstract GVls.Container keywords { owned get; }
  /**
   * Adds this server to given server and sets {@link root_server}
   */
  public abstract void add_to_root_server (Server rserver) throws GLib.Error;
  /**
   *
   */
  public abstract void parse (GLib.File file) throws GLib.Error;
  /**
   *
   */
  public abstract void parse_string (string str) throws GLib.Error;
  /**
   *
   */
  public abstract Symbol? get_symbol (string name);
  /**
   * Add a new server to the stack.
   */
  public virtual void add_server (Server s) throws GLib.Error
    requires (servers != null)
  {
    var k = new GContainerKeyString ();
    k.key = s.hash ();
    var isr = servers.find (k);
    if (isr != null) return;
    s.add_to_root_server (this);
  }
  public abstract Symbol? find_at (Location loc) throws GLib.Error;
  /**
   *
   */
  public abstract GLib.File prefix_dir { get; set; }
  /**
   * A container with {@link GLib.File} objects pointing to
   * directories to search packages files on.
   */
  public abstract GVls.Container vapi_dirs { get; }
  /**
   *
   */
  public abstract Server create_pkg_server (GLib.File file) throws GLib.Error;
  /**
   *
   */
  public virtual void add_pkg (string pkg) throws GLib.Error {
    var pkgf = find_package_file (pkg);
    if (pkgf != null) {
      add_server (create_pkg_server (pkgf));
    }
  }
  /**
   *
   */
  public virtual void add_default_namespaces () throws GLib.Error {
    add_pkg ("glib-2.0");
    add_pkg ("gio-2.0");
    add_pkg ("gobject-2.0");
  }
  /**
   * For current Vala API, add search directories for namespaces.
   */
  public virtual void add_default_vapi_dirs () throws GLib.Error
    requires (prefix_dir != null)
    requires (vapi_dirs != null)
  {
    if (prefix_dir.query_exists (null)) {
      vapi_dirs.add (prefix_dir);
    }
    var data_vapi = GLib.File.new_for_path (VAPIDATADIR);
    if (data_vapi.query_exists (null)) {
      vapi_dirs.add (data_vapi);
    }
  }
  /**
   *
   */
  public virtual GLib.File? find_package_file (string pkg) throws GLib.Error
    requires (prefix_dir != null)
    requires (vapi_dirs != null)
  {
    if (vapi_dirs.get_n_items () == 0) {
      add_default_vapi_dirs ();
    }
    for (int i = 0; i < vapi_dirs.get_n_items (); i++) {
      var d = vapi_dirs.get_item (i) as GLib.File;
      var pkgf = GLib.File.new_for_uri (d.get_uri ()+"/"+pkg+".vapi");
      if (pkgf.query_exists (null)) {
        return pkgf;
      }
    }
    return null;
  }
  /**
   *
   */
  public virtual GLib.File? find_namespace_file (string ns) throws GLib.Error
    requires (vapi_dirs != null)
  {
    if (vapi_dirs.get_n_items () == 0) {
      add_default_vapi_dirs ();
    }
    GLib.File nsf = null;
    for (int i = 0; i < vapi_dirs.get_n_items (); i++) {
      var d = vapi_dirs.get_item (i) as GLib.File;
      try {
        var fs = d.enumerate_children (FileAttribute.STANDARD_NAME,
                                        FileQueryInfoFlags.NONE, null);
        FileInfo info = fs.next_file (null);
        var pat = "^namespace(\\s)*"+ns+"(\\s{1})";
        GLib.Regex rex = new GLib.Regex (pat, RegexCompileFlags.OPTIMIZE, RegexMatchFlags.ANCHORED);
        while (info != null && nsf == null) {
          if (info.get_name ().has_suffix(".vapi")) {
            var f = fs.get_child (info);
            var istream = new DataInputStream (f.read ());
            string l = istream.read_line ();
            while (l != null) {
              MatchInfo match = null;
              if (rex.match (l, RegexMatchFlags.ANCHORED, out match)) {
                  nsf = f;
                  break;
              }
              l = istream.read_line ();
            }
          }
          info = fs.next_file (null);
        }
      } catch (GLib.Error e) {
        warning ("Error getting VAPI files at: %s : Error: %s", d.get_uri (), e.message);
      }
    }
    return nsf;
  }
  /**
   *
   */
  public virtual bool add_namespace (string ns) throws GLib.Error {
    if (root_server != null) {
      if (root_server.get_symbol (ns) != null) {
        return false;
      }
    } else if (get_symbol (ns) != null) {
      return false;
    }
    GLib.File nsf = null;
    if (root_server != null) {
      if (ns == "GLib") {
        root_server.add_default_namespaces ();
        return true;
      }
      root_server.find_namespace_file (ns);
    } else {
      if (ns == "GLib") {
        add_default_namespaces ();
        return true;
      }
      nsf = find_namespace_file (ns);
    }
    if (nsf == null) {
      throw new ServerError.NO_VAPI_DIR_ERROR ("Namespace not found in current VAPI dirs: %s", ns);
    }
    var srv = create_pkg_server (nsf);
    if (root_server != null) {
      root_server.add_server (srv);
    } else {
      add_server (srv);
    }
    return true;
  }
  /**
   *
   */
  public virtual void add_vapi_dir (GLib.File file) throws GLib.Error
    requires (vapi_dirs != null)
  {
    var i = file.query_info ("standard::*", GLib.FileQueryInfoFlags.NONE, null);
    if (i.get_file_type () != GLib.FileType.DIRECTORY) {
      throw new ServerError.NO_VAPI_DIR_ERROR ("Given file is not a directory. Can't be added as a VAPI directory");
    }
    vapi_dirs.add (file);
  }
  /**
   *
   */
  public abstract GLib.ListModel find_using_namespaces ();
  /**
   *
   */
  public virtual void add_using_namespaces () throws GLib.Error {
    var nss = find_using_namespaces ();
    if (!nss.get_item_type ().is_a (typeof (StringObject))) {
      warning ("List of namespaces in use should be StringObject derived types");
      return;
    }
    for (int i = 0; i < nss.get_n_items (); i++) {
      var ons = nss.get_item (i) as StringObject;
      try {
        add_namespace (ons.val);
      } catch (GLib.Error e) {
        message ("Error adding namespace: %s: %s", ons.val, e.message);
      }
    }
  }
  /**
   *
   */
  public virtual void add_all_using_namespaces () throws GLib.Error {
    for (int i = 0; i < servers.get_n_items (); i++) {
      var item = servers.get_item (i) as Server;
      if (item == null) continue;
      var l = item.find_using_namespaces ();
      if (!l.get_item_type ().is_a (typeof (StringObject))) {
        warning ("List of namespaces in use should be StringObject derived types");
        return;
      }
      for (int j = 0; j < l.get_n_items (); j++) {
        var ns = l.get_item (j) as StringObject;
        try {
          add_namespace (ns.val);
        } catch (GLib.Error e) {
          message ("Error adding namespace: %s: %s", ns.val, e.message);
        }
      }
    }
  }
  /**
   *
   */
  public virtual Container document_symbols {
    owned get {
      var l = new GContainer ();
      for (int i = 0; i < symbols.get_n_items (); i++) {
        var it = symbols.get_item (i) as Symbol;
        if (it == null) continue;
        l.add (it);
        child_document_symbols (it, l);
      }
      var ks = keywords;
      for (int i = 0; i < ks.get_n_items (); i++) {
        var kt = ks.get_item (i) as DocumentSymbol;
        if (kt == null) continue;
        l.add (kt);
      }
      return l;
    }
  }
  private static void child_document_symbols (Symbol sym, Container l) {
    for (int i = 0; i < sym.get_n_items (); i++) {
        var it = sym.get_item (i) as Symbol;
        if (it == null) continue;
        l.add (it);
        for (int j = 0; j < (it as Container).get_n_items (); j++) {
          var itl = (it as Container).get_item (j) as Symbol;
          if (itl == null) continue;
          l.add (itl);
          child_document_symbols (itl, l);
        }
      }
  }
}

public errordomain GVls.ServerError {
  NO_FILE_ERROR,
  NO_VAPI_DIR_ERROR,
  PARSER_ERROR
}
