/* gvls-client-lsp.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;
using GLib;
using Json;

public interface GVls.ClientLsp : GLib.Object
{
  public abstract Jsonrpc.Client rcp_client { get; }
  public abstract Workspace workspace { get; internal set; }
  public abstract bool initialized { get; internal set; }
  public abstract Container request { get; }
  public abstract Cancellable cancellable { get; set; }

  public virtual void connect (IOStream stream, string? ws_path) throws GLib.Error
    requires (workspace != null)
  {
    var rqi = new RequestItem ("initialize");
    var pars = new GInitializeParams ();
    pars.root_uri = ws_path;
    string pars_str = Json.gobject_to_data (pars, null);
    message (pars_str);
    Variant @params = new Variant.string (pars_str);
    Variant response = null;
    rcp_client.call (rqi.method, @params, cancellable, out response);
  }
}


public class GVls.RequestItem : GLib.Object, ContainerHashable {
  public string method { get; set; }
  public string id { get; set; }
  
  public RequestItem (string method) {
    this.method = method;
    this.id = Uuid.string_random ();
  }

  public string hash () { return id; }
  public bool equal (GLib.Object obj) {
    if (!(obj is RequestItem)) return false;
    if ((obj as RequestItem).id == id) return true;
    return false;
  }
}

public class GVls.GInitializeParams : GLib.Object, InitializeParams {
  public int process_id { get; set; }

  [Version (deprecated=true)]
  public string? root_path { get; set; }
  public string root_uri { get; set; }
  public GLib.Object? initialization_options { get; set; }
  public ClientCapabilities capabilities { get; set; }
  public string? trace { get; set; }
  public Container? workspaceFolders { get; set; }
  
  construct {
    root_path = null;
    root_uri = "file:///stdin";
    capabilities = new GClientCapabilities ();
  }

}

public class GVls.GClientCapabilities : GLib.Object, ClientCapabilities {
  [Description (nick="workspace")]
  public WorkspaceClientCapabilities? workspace { get; set; }
  [Description (nick="textDocument")]
  public TextDocumentClientCapabilities text_document { get; set; }
  [Description (nick="experimental")]
  public GLib.Object experimental { get; set; }
  construct {
    workspace = new GWorkspaceClientCapabilities ();
    text_document = new GTextDocumentClientCapabilities ();
  }
}

public class GVls.GWorkspaceClientCapabilities : GLib.Object, WorkspaceClientCapabilities {
  [Description (nick="applyEdit")]
  public bool apply_edit { get; set; }
  [Description (nick="workspaceEdit")]
  public WorkspaceClientCapabilities.WorkspaceEdit? workspace_edit { get; set; }
  [Description (nick="didChangeConfiguration")]
  public WorkspaceClientCapabilities.DidChangeConfiguration? did_change_configuration { get; set; }
  [Description (nick="didChangeWhatchedFile")]
  public WorkspaceClientCapabilities.DidChangeWatchedFiles? did_change_watched_files { get; set; }
  [Description (nick="symbol")]
  public WorkspaceClientCapabilities.Symbol symbol { get; set; }
  [Description (nick="executeCommand")]
  public WorkspaceClientCapabilities.ExecuteCommand execute_command { get; set; }
  [Description (nick="workspaceFolders")]
  public bool workspace_folders { get; set; }
  [Description (nick="configuration")]
  public bool configuration { get; set; }
}


public class GVls.GTextDocumentClientCapabilities : GLib.Object, TextDocumentClientCapabilities {
  [Description (nick="synchronization")]
  public TextDocumentClientCapabilities.Synchronization synchronization { get; set; }
  [Description (nick="completion")]
  public TextDocumentClientCapabilities.Completion completion  { get; set; }
  [Description (nick="hover")]
  public TextDocumentClientCapabilities.Hover hover { get; set; }
  [Description (nick="signatureHelp")]
  public TextDocumentClientCapabilities.SignatureHelp signature_help { get; set; }
  [Description (nick="references")]
  public TextDocumentClientCapabilities.References references { get; set; }
  [Description (nick="documentHighlight")]
  public TextDocumentClientCapabilities.DocumentHighlight document_highlight { get; set; }
  [Description (nick="documentSymbol")]
  public TextDocumentClientCapabilities.DocumentSymbol documentSymbol { get; set; }
  [Description (nick="formatting")]
  public TextDocumentClientCapabilities.Formating formatting { get; set; }
  [Description (nick="rangeFormatting")]
  public TextDocumentClientCapabilities.RangeFormatting range_formatting { get; set; }
  [Description (nick="onTypeFormatting")]
  public TextDocumentClientCapabilities.OnTypeFormatting on_type_formatting { get; set; }
  [Description (nick="definition")]
  public TextDocumentClientCapabilities.Definition definition { get; set; }
  [Description (nick="typeDefinition")]
  public TextDocumentClientCapabilities.TypeDefinition type_definition { get; set; }
  [Description (nick="implementation")]
  public TextDocumentClientCapabilities.Implementation implementation { get; set; }
  [Description (nick="codeAction")]
  public TextDocumentClientCapabilities.CodeAction codeAction { get; set; }
  [Description (nick="codeLens")]
  public TextDocumentClientCapabilities.CodeLens code_lens { get; set; }
  [Description (nick="documentLink")]
  public TextDocumentClientCapabilities.DocumentLink document_link { get; set; }
  [Description (nick="colorProvider")]
  public TextDocumentClientCapabilities.ColorProvider colorProvider { get; set; }
  [Description (nick="rename")]
  public TextDocumentClientCapabilities.Rename rename { get; set; }
  [Description (nick="publishDiagnostics")]
  public TextDocumentClientCapabilities.PublishDiagnostics publish_diagnostics { get; set; }
  [Description (nick="foldingRange")]
  public TextDocumentClientCapabilities.FoldingRange foldingRange { get; set; }
  construct {
    completion = new TextDocumentClientCapabilities.Completion ();
    documentSymbol = new TextDocumentClientCapabilities.DocumentSymbol ();
  }
}

