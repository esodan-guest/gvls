/* gvls-workspace.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.InitializeParams : Object {
  /**
   * The process Id of the parent process that started
   * the server. Is null if the process has not been started by another process.
   * If the parent process is not alive then the server should exit (see exit notification) its process.
   */
  public abstract int process_id { get; set; }

  /**
   * The rootPath of the workspace. Is null
   * if no folder is open.
   *
   * Deprecated: in favour of rootUri.
   */
  [Version (deprecated=true)]
  public abstract string? root_path { get; set; }

  /**
   * The rootUri of the workspace. Is null if no
   * folder is open. If both `rootPath` and `rootUri` are set
   * `rootUri` wins.
   */
  public abstract string root_uri { get; set; }

  /**
   * User provided initialization options.
   */
  public abstract Object? initialization_options { get; set; }

  /**
   * The capabilities provided by the client (editor or tool)
   */
  public abstract ClientCapabilities capabilities { get; set; }

  /**
   * The initial trace setting. If omitted trace is disabled ('off').
   *
   * Valid values are: 'off' | 'messages' | 'verbose'
   */
  public abstract string? trace { get; set; }

  /**
   * The workspace folders configured in the client when the server starts.
   * This property is only available if the client supports workspace folders.
   * It can be `null` if the client supports workspace folders but none are
   * configured.
   *
   * A container with objects of type {@link WorkspaceFolder}
   *
   * Since 3.6.0
   */
  public abstract Container? workspaceFolders { get; set; }
}

public interface GVls.InitializeResult : Object {
  /**
   * The capabilities the language server provides.
   */
  public abstract ServerCapabilities capabilities { get; set; }
}

  /**
  * Known error codes for an `InitializeError`;
  */
public enum GVls.InitializeError {
  UNKNOWN,
  /**
   * If the protocol version provided by the client can't be handled by the server.
   * 
   * Deprecated: This initialize error got replaced by client capabilities. There is
   * no version handshake in version 3.0x
   */
  [Version (deprecated=true)]
  UNKNOWN_PROTOCOL_VERSION;
  public string to_string () {
    string str = "";
    switch (this) {
      case UNKNOWN_PROTOCOL_VERSION:
        str = "unknownProtocolVersion";
        break;
      case UNKNOWN:
        str = "unknown";
        break;
    }
    return str;
  }
}

public class GVls.InitializeErrorObject : GVls.EnumObject {
  construct {
    enum_type = typeof (InitializeError);
  }
  public InitializeErrorObject (InitializeError kind) {
    val = kind;
  }
  public InitializeError get_enum () { return (InitializeError) val; }
  public override string to_string () { return ((InitializeError) val).to_string (); }
}


public interface GVls.ClientCapabilities : Object {
  /**
   * Workspace specific client capabilities.
   */
  [Description (nick="workspace")]
  public abstract WorkspaceClientCapabilities? workspace { get; set; }

  /**
   * Text document specific client capabilities.
   */
  [Description (nick="textDocument")]
  public abstract TextDocumentClientCapabilities text_document { get; set; }

  /**
   * Experimental client capabilities.
   */
  [Description (nick="experimental")]
  public abstract GLib.Object experimental { get; set; }
}

public enum GVls.ResourceOperationKind {
  UNKNOWN,
  CREATE,
  RENAME,
  DELETE;
  public string to_string () {
    string str = "";
    switch (this) {
      case CREATE:
        str = "create";
        break;
      case RENAME:
        str = "rename";
        break;
      case DELETE:
        str = "delete";
        break;
      case UNKNOWN:
        str = "unknown";
        break;
    }
    return str;
  }
}

public class GVls.ResourceOperationKindObject : GVls.EnumObject {
  construct {
    enum_type = typeof (ResourceOperationKind);
  }
  public ResourceOperationKindObject (ResourceOperationKind kind) {
    val = kind;
  }
  public ResourceOperationKind get_enum () { return (ResourceOperationKind) val; }
  public override string to_string () { return ((ResourceOperationKind) val).to_string (); }
}

public enum GVls.FailureHandlingKind {
  UNKNOWN,
  ABORT,
  TRANSACTIONAL,
  TEXT_ONLY_TRANSACTIONAL,
  UNDO;
  public string to_string () {
    string str = "";
    switch (this) {
      case ABORT:
        str = "abort";
        break;
      case TRANSACTIONAL:
        str = "transactional";
        break;
      case TEXT_ONLY_TRANSACTIONAL:
        str = "textOnlyTransactional";
        break;
      case UNDO:
        str = "undo";
        break;
      case UNKNOWN:
        str = "unknown";
        break;
    }
    return str;
  }
}

/**
 * Workspace specific client capabilities.
 */
public interface GVls.WorkspaceClientCapabilities : Object {
  /**
   * The client supports applying batch edits to the workspace by supporting
   * the request 'workspace/applyEdit'
   */
  [Description (nick="applyEdit")]
  public abstract bool apply_edit { get; set; }
  /**
   * Capabilities specific to `WorkspaceEdit`s
   */
  [Description (nick="workspaceEdit")]
  public abstract WorkspaceEdit? workspace_edit { get; set; }
  public class WorkspaceEdit : Object {
    construct {
      resource_operations = new GContainer ();
      resource_operations.add (new ResourceOperationKindObject (ResourceOperationKind.CREATE));
      resource_operations.add (new ResourceOperationKindObject (ResourceOperationKind.RENAME));
      resource_operations.add (new ResourceOperationKindObject (ResourceOperationKind.DELETE));
    }
    /**
     * The client supports versioned document changes in `WorkspaceEdit`s
     */
    [Description (nick="documentChanges")]
    public bool document_changes { get; set; }

    /**
     * The resource operations the client supports. Clients should at least
     * support 'create', 'rename' and 'delete' files and folders.
     *
     * Container hold objects of type {@link ResourceOperationKindObject}
     */
    [Description (nick="resourceOperations")]
    public Container? resource_operations { get; set; }

    /**
     * The failure handling strategy of a client if applying the workspace edit
     * fails.
     */
    [Description (nick="failureHandling")]
    public FailureHandlingKind failure_handling { get; set; }
  }

  /**
   * Capabilities specific to the `workspace/didChangeConfiguration` notification.
   */
  [Description (nick="didChangeConfiguration")]
  public abstract DidChangeConfiguration? did_change_configuration { get; set; }
  public class DidChangeConfiguration : Object {
    /**
     * Did change configuration notification supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `workspace/didChangeWatchedFiles` notification.
   */
  [Description (nick="didChangeWathedFiles")]
  public abstract DidChangeWatchedFiles? did_change_watched_files { get; set; }
  public class DidChangeWatchedFiles : Object {
    /**
     * Did change watched files notification supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `workspace/symbol` request.
   */
  [Description (nick="symbol")]
  public abstract Symbol symbol { get; set; }
  public class Symbol : Object {
    /**
     * Symbol request supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }

    /**
     * Specific capabilities for the `SymbolKind` in the `workspace/symbol` request.
     */
    [Description (nick="symbolKind")]
    public SymbolKind symbol_kind { get; set; }
  }
  public class SymbolKind : Object {
    /**
     * The symbol kind values the client supports. When this
     * property exists the client also guarantees that it will
     * handle values outside its set gracefully and falls back
     * to a default value when unknown.
     *
     * If this property is not present the client only supports
     * the symbol kinds from `File` to `Array` as defined in
     * the initial version of the protocol.
     *
     * Container holds items of type {@link GVls.SymbolKindObject}
     */
    [Description (nick="valueSet")]
    public Container value_set { get; set; }
  }

  /**
   * Capabilities specific to the `workspace/executeCommand` request.
   */
    [Description (nick="executeCommand")]
  public abstract ExecuteCommand execute_command { get; set; }
  public class ExecuteCommand : Object {
    /**
     * Execute command supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * The client has support for workspace folders.
   *
   * Since 3.6.0
   */
  [Description (nick="workspaceFolders")]
  public abstract bool workspace_folders { get; set; }

  /**
   * The client supports `workspace/configuration` requests.
   *
   * Since 3.6.0
   */
  [Description (nick="configuration")]
  public abstract bool configuration { get; set; }
}
