/* gvls-code-commands.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.CodeActions : Object {
  public signal void code_action_request (GVls.RequestMessage msg);
  public virtual void report_code_action (GVls.RequestMessage msg, CodeActionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/codeAction";
    msg.@params.add (@params);
    code_action_request (msg);
  }
}

/**
 * Params for the CodeActionRequest
 */
public interface GVls.CodeActionParams : Object {
  /**
   * The document in which the command was invoked.
   */
  public abstract TextDocumentIdentifier textDocument { get; set; }

  /**
   * The range for which the command was invoked.
   */
  public abstract Range range { get; set; }

  /**
   * Context carrying additional information.
   */
  public abstract CodeActionContext context { get; set; }
}

/**
 * The kind of a code action.
 *
 * Kinds are a hierarchical list of identifiers separated by `.`, e.g. `"refactor.extract.function"`.
 *
 * The set of kinds is open and client needs to announce the kinds it supports to the server during
 * initialization.
 *
 * A set of predefined code action kinds
 */
public enum GVls.CodeActionKind {
  UNKNOWN,
  /**
   * Base kind for quickfix actions: 'quickfix'
   */
  QUICK_FIX,

  /**
   * Base kind for refactoring actions: 'refactor'
   */
  REFACTOR,

  /**
   * Base kind for refactoring extraction actions: 'refactor.extract'
   *
   * Example extract actions:
   *
   * - Extract method
   * - Extract function
   * - Extract variable
   * - Extract interface from class
   * - ...
   */
  REFACTOR_EXTRACT,

  /**
   * Base kind for refactoring inline actions: 'refactor.inline'
   *
   * Example inline actions:
   *
   * - Inline function
   * - Inline variable
   * - Inline constant
   * - ...
   */
  REFACTOR_INLINE,

  /**
   * Base kind for refactoring rewrite actions: 'refactor.rewrite'
   *
   * Example rewrite actions:
   *
   * - Convert JavaScript function to class
   * - Add or remove parameter
   * - Encapsulate field
   * - Make method static
   * - Move method to base class
   * - ...
   */
  REFACTOR_REWRITE,

  /**
   * Base kind for source actions: `source`
   *
   * Source code actions apply to the entire file.
   */
  SOURCE,

  /**
   * Base kind for an organize imports source action: `source.organizeImports`
   */
  SOURCE_ORGANIZE_IMPORTS;



  public string to_string () {
    string str = "";
    switch (this) {
      case QUICK_FIX:
        str = "quickfix";
        break;
      case REFACTOR:
        str = "refactor";
        break;
      case REFACTOR_EXTRACT:
        str = "refactor.extract";
        break;
      case REFACTOR_INLINE:
        str = "refactor.inline";
        break;
      case REFACTOR_REWRITE:
        str = "refactor.rewrite";
        break;
      case SOURCE:
        str = "source";
        break;
      case SOURCE_ORGANIZE_IMPORTS:
        str = "source.organizeImports";
        break;
    }
    return str;
  }
}

public class GVls.CodeActionKindObject : GVls.EnumObject {
  construct {
    enum_type = typeof (CodeActionKind);
  }
  public CodeActionKindObject (CodeActionKind kind) {
    val = kind;
  }
  public CodeActionKind get_enum () { return (CodeActionKind) val; }
  public override string to_string () { return ((CodeActionKind) val).to_string (); }
}


/**
 * Contains additional diagnostic information about the context in which
 * a code action is run.
 */
public interface GVls.CodeActionContext : Object {
	/**
	 * An array of diagnostics of type {@link Diagnostic}
	 */
	public abstract Container diagnostics { get; }

	/**
	 * Requested kind of actions to return.
	 *
	 * Actions not of this kind are filtered out by the client before being shown. So servers
	 * can omit computing them.
	 *
	 * Container's items are of type {@link CodeActionKindObject}
	 */
	public abstract Container only { get; }
}

/**
 * A code action represents a change that can be performed in code, e.g. to fix a problem or
 * to refactor code.
 *
 * A CodeAction must set either `edit` and/or a `command`. If both are supplied, the `edit` is applied first, then the `command` is executed.
 */
public interface GVls.CodeAction : Object {

	/**
	 * A short, human-readable, title for this code action.
	 */
	public abstract string title { get; set; }

	/**
	 * The kind of the code action.
	 *
	 * Used to filter code actions.
	 */
	public abstract CodeActionKind kind { get; set; }

	/**
	 * The diagnostics that this code action resolves.
	 *
	 * Container's items are of type {@link Diagnostic}
	 */
	public abstract Container diagnostics { get; set; }

	/**
	 * The workspace edit this code action performs.
	 */
	public abstract WorkspaceEdit edit { get; set; }

	/**
	 * A command this code action executes. If a code action
	 * provides an edit and a command, first the edit is
	 * executed and then the command.
	 */
	public abstract Command command { get; set; }
}

public interface GVls.CodeActionRegistrationOptions : Object,
                                                      TextDocumentRegistrationOptions,
                                                      CodeActionOptions {}

public interface GVls.WorkspaceEdit : Object {
  /**
   * Holds changes to existing resources.
   */
  public abstract WorkspaceEditChanges changes { get; }

  /**
   * Depending on the client capability `workspace.workspaceEdit.resourceOperations` document changes
   * are either an array of `TextDocumentEdit`s to express changes to n different text documents
   * where each text document edit addresses a specific version of a text document. Or it can contain
   * above `TextDocumentEdit`s mixed with create, rename and delete file / folder operations.
   *
   * Whether a client supports versioned document edits is expressed via
   * `workspace.workspaceEdit.documentChanges` client capability.
   *
   * If a client neither supports `documentChanges` nor `workspace.workspaceEdit.resourceOperations` then
   * only plain `TextEdit`s using the `changes` property are supported.
   */
  public abstract Object documentChanges { get; set; }
}

public class GVls.WorkspaceEditChanges : Object {
  private Container _edits = new GContainer ();
  public string uri { get; set; }
  /**
   * A container with all {@link TextEdit} objects related
   * with the changes.
   */
  public Container edits  { get { return _edits; } }
}


/**
 * Code Action options.
 */
public interface GVls.CodeActionOptions : Object {
  /**
   * CodeActionKinds that this server may return.
   *
   * The list of kinds may be generic, such as `CodeActionKind.Refactor`, or the server
   * may list out every specific kind they provide.
   *
   * Container's items are of type {@link CodeActionKindObject}
   */
  public abstract Container codeActionKinds { get; }
}
