/* gvls-gstring-literal.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vala;
using GLib;

public class GVls.GStringLiteral : GVls.GStatement
{
  construct {
    _location = new GLocation ();
    _name = "@STRINGLITERAL@";
    _kind = SymbolKind.STRING;
  }
  public GStringLiteral (Location loc) {
    _location = loc;
    _name = "@STRINGLITERAL@" + GLib.Uuid.string_random ();
  }
}
