/* gvls-response-error.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.ResponseError : Object {
  public abstract int code { get; }
  public abstract string message { get; }
  public abstract Object? data { get; }
  public abstract Type data_type { get; }
  public enum Codes {
    PARSE_ERROR = -32700,
    INVALID_REQUEST = -32600,
    METHOD_NOT_FOUND = -32601,
    INVALID_PARAMS = -32602,
    INTERNAL_ERROR = -32603,
    SERVER_ERROR_START = -32099,
    SERVER_ERROR_END = -32000,
    SERVER_NOT_INITIALIZED = -32002,
    UNKNOWN_ERROR_CODE = -32001,

    // Defined by the protocol.
    REQUEST_CANCELLED = -32800;
    public string to_string () {
      string str = "";
      switch (this) {
        case PARSE_ERROR:
          str = "ParseError";
          break;
        case INVALID_REQUEST:
          str = "InvalidRequest";
          break;
        case METHOD_NOT_FOUND:
          str = "MethodNotFound";
          break;
        case INVALID_PARAMS:
          str = "InvalidParams";
          break;
        case INTERNAL_ERROR:
          str = "InternalError";
          break;
        case SERVER_ERROR_START:
          str = "serverErrorStart";
          break;
        case SERVER_ERROR_END:
          str = "serverErrorEnd";
          break;
        case SERVER_NOT_INITIALIZED:
          str = "ServerNotInitialized";
          break;
        case REQUEST_CANCELLED:
          str = "RequestCancelled";
          break;
        case UNKNOWN_ERROR_CODE:
        default:
          str = "UnknownErrorCode";
          break;
      }
      return str;
    }
  }
}
