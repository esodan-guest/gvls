/* gvls-text-document-open.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.DidOpenTextDocumentParams : Object {
  /**
   * The document that was opened.
   */
  public abstract TextDocumentItem text_document { get; set; }
}

public interface GVls.TextDocumentItem : Object {
  public abstract GLib.File file { get; set; }
  /**
   * The text document's URI.
   */
  public string uri { owned get { return file.get_uri (); } }

  /**
   * The text document's language identifier.
   */
  public abstract string language_id { get; set; }

  /**
   * The version number of this document (it will increase after each
   * change, including undo/redo).
   */
  public abstract int version { get; set; }

  /**
   * The content of the opened text document.
   */
  public abstract string text { get; }
}

