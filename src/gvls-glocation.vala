/* gvls-glocation.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vala;

public class GVls.GLocation : GLib.Object, Range, Location
{
  // Location
  private GLib.File _file = GLib.File.new_for_uri ("file:///stdin");
  public GLib.File file { get { return _file; } }
  // Range
  private Position _start;
  private Position _end;
  public Position start { get { return _start; } }
  public Position end { get { return _end; }  }

  construct {
    _start = new GPosition ();
    _end = new GPosition ();
  }

  public GLocation.from_values (GLib.File file, int start_line, int start_column, int end_line, int end_column) {
    _start = new GPosition.from_values (start_line, start_column);
    _end = new GPosition.from_values (end_line, end_column);
    _file = file;
  }
}


public class GVls.GPosition : Object, Position {
  int _line = -1;
  int _character = -1;
  public int line {
    get {
      return _line;
    }
  }
  public int character {
    get {
      return _character;
    }
  }
  public GPosition.from_values (int line, int character) {
    _line = line;
    _character = character;
  }
}
