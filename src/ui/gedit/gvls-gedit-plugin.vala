/* gvls-sourceview.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;

namespace GVlsEdit {
  public class Window : Peas.ExtensionBase, Gedit.WindowActivatable {
    private GVls.Server _server;
    private ulong cn = 0;
    private uint timeout_id = -1;

    public Gedit.Window window { construct; owned get; }

    construct {
      _server = new GVls.GServer ();
      try {
        _server.add_default_vapi_dirs ();
      } catch (GLib.Error e) {
        warning ("Initialization Error: %s", e.message);
      }
    }
    ~Window () {
      if (timeout_id != -1) {
        var source = MainContext.@default ().find_source_by_id (timeout_id);
        if (source != null) {
          source.destroy ();
        }
      }
    }

    public void activate () {
      cn = window.tab_added.connect ((tab)=>{
        message ("New Tab Added");
        var doc = tab.get_document ();
        doc.loaded.connect (()=>{
          init_doc (doc);
        });
      });
      timeout_id = GLib.Timeout.add (500, update_symbols);
    }
    public void deactivate () {
        window.disconnect (cn);
    }
    public void update_state () {
    }
    private void init_doc (Gedit.Document doc) {
        var tab = Gedit.Tab.get_from_document (doc);
        var view = tab.get_view ();
        var prov = view.get_data<GVlsui.CompletionProvider> ("gvls-provider");
        if (prov == null) return;
        try  {
          prov.server = _server;
          prov.current_server.parse (doc.get_file ().location);
        } catch (GLib.Error e) {
          message ("Error parsing document: %s", e.message);
        }
    }

    private bool update_symbols () {
      var view = window.get_active_view ();
      if (view == null) return true;
      var prov = view.get_data<GVlsui.CompletionProvider> ("gvls-provider");
      if (prov == null) return true;
      bool dirty = view.get_data<bool> ("gvls-view-dirty");
      if (!dirty) return true;
      prov.current_server.content = view.get_buffer ().text;
      view.set_data<bool> ("gvls-view-dirty", false);
      return true;
    }
  }
  public class View : Peas.ExtensionBase, Gedit.ViewActivatable
  {
    GVlsui.CompletionProvider prov = new GVlsui.CompletionProvider ();

    public Gedit.View view {
      owned get; construct;
    }

    public void activate () {
      prov = new GVlsui.CompletionProvider ();
      try {
        view.get_completion ().add_provider (prov);
        view.set_data<GVlsui.CompletionProvider> ("gvls-provider", prov);
        view.set_data<bool> ("gvls-view-dirty", true);
        var buf = view.get_buffer ();
        buf.insert_text.connect ((ref pos, ntext, tlen)=>{
          view.set_data<bool> ("gvls-view-dirty", true);
        });
      } catch (GLib.Error e) {
        warning ("Error setting completion provider: %s", e.message);
      }
    }

    public void deactivate () {
      try {
        view.get_completion ().remove_provider (prov);
      } catch (GLib.Error e) {
        warning ("Deactivation Error: %s", e.message);
      }
    }
  }

  /*
   * Plugin config dialog
   */
  public class Config : Peas.ExtensionBase, PeasGtk.Configurable
  {
    public Gtk.Widget create_configure_widget ()
    {
      return new Gtk.Label ("GVls Gedit 3.0 Vala Language Completion Plugin ");
    }
  }
}

[ModuleInit]
public void peas_register_types (TypeModule module)
{
  var objmodule = module as Peas.ObjectModule;

  // Register my plugin extension
  objmodule.register_extension_type (typeof (Gedit.ViewActivatable), typeof (GVlsEdit.View));
  objmodule.register_extension_type (typeof (Gedit.WindowActivatable), typeof (GVlsEdit.Window));
  // Register my config dialog
  objmodule.register_extension_type (typeof (PeasGtk.Configurable), typeof (GVlsEdit.Config));
}

