/* gvls-sourceview.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GVlsui;
using Gtk;
using Pango;

public class GVlsui.SourceView : Gtk.SourceView
{

  private GVls.Server _server;
  private GVlsui.CompletionProvider _prov;
  private uint timed_id = -1;
  private bool dirty = true;

  public GVls.Server server {
    get { return _server; }
    set {
      _server = value;
      _prov.server = _server;
      get_buffer ().create_tag ("bold", "weight", Pango.Weight.BOLD);
      get_buffer ().create_tag ("type", "weight", Pango.Weight.BOLD, "foreground", "#204a87");
      get_buffer ().create_tag ("keyword", "weight", Pango.Weight.BOLD, "foreground", "#a40000");
      get_buffer ().create_tag ("text", "weight", Pango.Weight.NORMAL, "foreground", "#729fcf");
      get_buffer ().create_tag ("number", "weight", Pango.Weight.BOLD, "foreground", "#ad7fa8");
      get_buffer ().create_tag ("method", "weight", Pango.Weight.BOLD, "foreground", "#729fcf");
    }
  }

  construct {
    editable = true;
    cursor_visible = true;
    monospace = true;
    var b = get_buffer () as Gtk.SourceBuffer;
    b.highlight_matching_brackets = true;
    b.highlight_syntax = true;
    _prov = new GVlsui.CompletionProvider ();
    _prov.current_server.content = "";
    try {
      timed_id = Timeout.add (1, update_document_symbols);
      get_completion ().add_provider (_prov);
      var buf = get_buffer ();
      buf.changed.connect (()=>{
        dirty = true;
      });
    } catch (GLib.Error e) {
      warning ("Error setting completion provider: %s", e.message);
    }
  }
  ~SourceView () {
    if (timed_id != -1) {
        var source = MainContext.@default ().find_source_by_id (timed_id);
        if (source != null) {
          source.destroy ();
        }
      }
  }

  public bool update_document_symbols () {
    if (!dirty) return true;
    _prov.current_server.content = get_buffer ().text;
    apply_syntax_highlight ();
    dirty = false;
    return true;
  }
  public void apply_syntax_highlight () {
    GVls.Container syms = _prov.current_server.document_symbols;
    Gtk.TextBuffer buffer = get_buffer ();
    Gtk.TextIter start, end;
    buffer.get_start_iter (out start);
    buffer.get_end_iter (out end);
    buffer.remove_all_tags (start, end);
    for (int i = 0; i < syms.get_n_items (); i++) {
      Symbol sym = syms.get_item (i) as Symbol;
      if (sym.range.start.line < 0 ||
          sym.range.start.character < 0 ||
          sym.range.end.line < 0 ||
          sym.range.end.character < 0) {
        continue;
      }
      buffer.get_iter_at_line_offset (out start, sym.range.start.line - 1, sym.range.start.character - 1);
      buffer.get_iter_at_line_offset (out end, sym.range.end.line - 1, sym.range.end.character);
      string current_text = get_buffer ().get_text (start, end, false);
      if (current_text != sym.name && sym .kind != GVls.SymbolKind.STRING) continue;
      switch (sym.kind) {
        case GVls.SymbolKind.KEY:
          get_buffer ().apply_tag_by_name ("keyword", start, end);
          break;
        case GVls.SymbolKind.CLASS:
        case GVls.SymbolKind.INTERFACE:
        case GVls.SymbolKind.STRUCT:
        case GVls.SymbolKind.ENUM:
        case GVls.SymbolKind.NAMESPACE:
          get_buffer ().apply_tag_by_name ("type", start, end);
          break;
        case GVls.SymbolKind.METHOD:
          get_buffer ().apply_tag_by_name ("method", start, end);
          break;
        case GVls.SymbolKind.STRING:
          get_buffer ().apply_tag_by_name ("text", start, end);
          break;
      }
    }
  }
}
