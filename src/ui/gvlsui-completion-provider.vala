/* gvls-completion-provider.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GVls;
using Gtk;

public class GVlsui.CompletionProvider : GLib.Object, Gtk.SourceCompletionProvider
{
  private GVls.Server _server;
  private GVls.Server _current_server;

  public GVls.Server server {
    get { return _server; }
    set {
      _server = value;
      try {
        _server.add_server (current_server);
        assert (current_server.root_server != null);
      } catch (GLib.Error e) {
        message ("Error setting completion provider's server': %s", e.message);
      }
    }
  }
  public GVls.Server current_server {
    get { return _current_server; }
  }

  construct {
    _current_server = new GServer ();
  }

  // Gtk.SourceCompletionProvider
  public string get_name () { return "GVls"; }
  public void populate (Gtk.SourceCompletionContext context) {
    if (server == null) {
      return;
    }
    Gtk.TextIter iter;
    if (!(context is Gtk.SourceCompletionContext)) {
      message ("GVls-Completion: Invalid Conext aborting...");
      return;
    }
    if (!context.get_iter (out iter)) {
      message ("GVls-Completion: Invalid iterator aborting...");
      return;
    }
    Gtk.TextIter end = Gtk.TextIter ();
    Gtk.TextIter istr = Gtk.TextIter ();
    end.assign (iter);
    istr.assign (iter);
    bool partial = false;
    bool puntfound = false;
    string sw = null;
    while (iter.backward_char ()) {
      unichar uc = iter.get_char ();
      if (uc.isspace ()) {
        iter.forward_char ();
        break;
      }
      if (uc.type () == UnicodeType.OPEN_PUNCTUATION) {
        if (uc.to_string () == "(") {
          iter.forward_char ();
          break;
        }
        if (uc.to_string () == "[") {
          iter.forward_char ();
          break;
        }
        if (uc.to_string () == "{") {
          iter.forward_char ();
          break;
        }
      }
      if (uc.type () == UnicodeType.OTHER_PUNCTUATION) {
        if (uc.to_string () == ";") {
          iter.forward_char ();
          break;
        }
      }
      if (uc.ispunct () && !puntfound) {
        if (uc.to_string () == ".") {
          istr.assign (iter);
          iter.forward_char ();
          sw = iter.get_visible_text (end);
          iter.backward_char ();
          puntfound = true;
        }
      }
    }
    string w = iter.get_visible_text (end);
    if ("\"" in w || w == "") {
      return;
    }
    message ("Searching completion for: %s", w);
    bool withdot = false;
    bool dotrequired = false;
    if (w.has_suffix (".")) {
      w = w.substring (0,w.length -1);
      withdot = true;
    }
    message ("Text to search symbol for: %s", w);
    GVls.Symbol list;
    list = server.get_symbol (w);
    if (list == null) {
      w = iter.get_visible_text (istr);
      message ("Searching Again for completion for: %s", w);
      list = server.get_symbol (w);
      withdot = partial = true;
    }
    if (list == null) {
      return;
    }
    GLib.List<Gtk.SourceCompletionProposal> l = new GLib.List<Gtk.SourceCompletionProposal> ();
    message ("Found symbol: %s", w);
    // Create a list of proposals
    switch (list.symbol_kind) {
      case SymbolKind.CLASS:
      case SymbolKind.INTERFACE:
      case SymbolKind.NAMESPACE:
      case SymbolKind.STRUCT:
      case SymbolKind.ENUM:
        dotrequired = true;
        break;
      case SymbolKind.METHOD:
        break;
      case SymbolKind.VARIABLE:
        dotrequired = true;
        var vtn = list.data_type;
        message ("Found Variable: %s of Type: %s",
                  list.name,
                  vtn);
        var vt = server.get_symbol (vtn);
        if (vt == null) {
          message ("Variable's Type not found: %s", vtn);
          return;
        }
        list = vt;
        break;
      default:
        break;
    }
    if (!withdot && dotrequired) {
      return;
    }
    for (int i = 0; i < list.get_n_items (); i++) {
      var item = list.get_item (i) as GVls.Symbol;
      if (partial && !(sw in item.name)) {
        continue;
      }
      var c = new SourceCompletionProposal (item);
      l.append (c);
    }
    context.add_proposals (this, l, true);
  }

  public class SourceCompletionProposal : GLib.Object, Gtk.SourceCompletionProposal
  {
    private GVls.Symbol _symbol;
    private GLib.Icon _icon;
    private GLib.Icon _icon_current;

    construct  {
      _icon = new ThemedIcon ("network-transmit");
    }
    public SourceCompletionProposal (GVls.Symbol symbol) {
      _symbol = symbol;
    }
    public unowned GLib.Icon? get_gicon () {
      switch (_symbol.symbol_kind) {
        case SymbolKind.CLASS:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/class.png"));
          break;
        case SymbolKind.INTERFACE:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/interface.png"));
          break;
        case SymbolKind.NAMESPACE:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/namespace.png"));
          break;
        case SymbolKind.STRUCT:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/struct.png"));
          break;
        case SymbolKind.ENUM:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/enum.png"));
          break;
        case SymbolKind.ENUM_MEMBER:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/enumvalue.png"));
          break;
        case SymbolKind.METHOD:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/method.png"));
          break;
        case SymbolKind.VARIABLE:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/field.png"));
          break;
        case SymbolKind.PROPERTY:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/property.png"));
          break;
        default:
          _icon_current = _icon;
          break;
      }
      return _icon_current;
    }
    public unowned string? get_icon_name () {
      return "symbol";
    }
    public string? get_info () { return ""; }
    public string get_label () {
      return _symbol.name;
    }
    public string get_markup () {
      // FIXME: markup text according with its type
      return _symbol.name;
    }
    public string get_text () {
      string str = _symbol.name;
      switch (_symbol.symbol_kind) {
        case SymbolKind.CLASS:
        case SymbolKind.INTERFACE:
        case SymbolKind.NAMESPACE:
        case SymbolKind.STRUCT:
        case SymbolKind.ENUM:
        case SymbolKind.ENUM_MEMBER:
        case SymbolKind.VARIABLE:
        case SymbolKind.PROPERTY:
          break;
        case SymbolKind.METHOD:
          str += " ()";
          break;
      }
      return str;
    }
  }
}
