/* gvls-text-document-will-save.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The parameters send in a will save text document notification.
 */
public interface GVls.WillSaveTextDocumentParams : Object {
  /**
   * The document that will be saved.
   */
  public abstract TextDocumentIdentifier text_document { get; set; }

  /**
   * The 'TextDocumentSaveReason'.
   */
  public abstract TextDocumentSaveReason reason { get; set; }
}

/**
 * Represents reasons why a text document is saved.
 */
public enum GVls.TextDocumentSaveReason {
  UNKNOWN,
  /**
   * Manually triggered, e.g. by the user pressing save, by starting debugging,
   * or by an API call.
   */
  MANUAL,

  /**
   * Automatic after a delay.
   */
  AFTER_DELAY,

  /**
   * When the editor lost focus.
   */
  FOCUS_OUT;

  public string to_string () {
    string str = "";
    switch (this) {
      case MANUAL:
        str = "Manual";
        break;
      case AFTER_DELAY:
        str = "AfterDelay";
        break;
      case FOCUS_OUT:
        str = "FocusOut";
        break;
    }
    return str;
  }
}

public interface GVls.DidSaveTextDocumentParams : Object {
  /**
   * The document that was saved.
   */
  public abstract TextDocumentIdentifier text_document { get; set; }

  /**
   * Optional the content when saved. Depends on the includeText value
   * when the save notification was requested.
   */
  public abstract string? text { get; set; }
}

