/* gvls-gscanner.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vala;

public class GVls.GScanner : Object {
  public signal void keyword (Symbol k);
  public signal void identifier (Location loc, string name);
  public signal void string_literal (Location loc);

  private GLib.File _file = null;
  private string _content = null;
  public GLib.File file { get { return _file; } }
  public string content { get { return _content; } }

  public GScanner.from_file (GLib.File file) {
    _file = file;
  }
  public GScanner.from_string (string str) {
    _content = str;
  }


  /**
   * Start sanning.
   *
   * Returns: a {@link Container} with a list of all {@link DocumentSymbol} found
   */
  public Container start (Server server) throws GLib.Error
  {
    var context = new CodeContext ();
    CodeContext.push (context);
    SourceFileType type = SourceFileType.NONE;
    string[] lines;
    if (file == null) {
      type = SourceFileType.SOURCE;
    } else {
      if (file.get_basename ().has_suffix (".vala")) {
        type = SourceFileType.SOURCE;
      }
      if (file.get_basename ().has_suffix (".vapi")) {
        type = SourceFileType.PACKAGE;
      }
      try {
        var ostream = new GLib.MemoryOutputStream.resizable ();
        var istream = file.read ();
        ostream.splice (istream, GLib.OutputStreamSpliceFlags.CLOSE_SOURCE, null);
        ostream.close ();
        _content = (string) ostream.steal_data ();
      } catch (GLib.Error e) {
        warning ("Error reading file contents: %s", e.message);
      }
    }
    if (type == SourceFileType.NONE) {
      throw new ScannerError.FILE_TYPE_ERROR ("File not supported: %s", file.get_uri ());
    }
    string filename = "file:///stdin";
    if (file != null) {
      filename = file.get_path ();
    }
    if (file == null && content == null) {
      throw new ScannerError.CONTENT_ERROR ("Content should not be null if no file is set");
    }
    var sf = new SourceFile (context, type, filename, content);
    var scanner = new Vala.Scanner (sf);
    var list = new GContainer ();
    lines = _content.split ("\n");
    // FIXME: Parse use parse_file_comments()
    Vala.TokenType token = Vala.TokenType.NONE;
    while (token != Vala.TokenType.EOF) {
      Vala.SourceLocation begin, end;
      token = scanner.read_token (out begin, out end);
      GLib.File f = file;
      if (f == null) {
        f = GLib.File.new_for_uri (filename);
      }
      var loc = new GLocation.from_values (f, begin.line, begin.column,
                                          end.line, end.column);
      string n = token.to_string ();
      n = n.replace ("`", "");
      n = n.replace ("'", "");
      switch (token) {
        case Vala.TokenType.ABSTRACT:
        case Vala.TokenType.AS:
        case Vala.TokenType.ASYNC:
        case Vala.TokenType.BASE:
        case Vala.TokenType.BREAK:
        case Vala.TokenType.CASE:
        case Vala.TokenType.CATCH:
        case Vala.TokenType.CLASS:
        case Vala.TokenType.CONST:
        case Vala.TokenType.CONSTRUCT:
        case Vala.TokenType.CONTINUE:
        case Vala.TokenType.DEFAULT:
        case Vala.TokenType.DELEGATE:
        case Vala.TokenType.DELETE:
        case Vala.TokenType.DO:
        case Vala.TokenType.DYNAMIC:
        case Vala.TokenType.ELSE:
        case Vala.TokenType.ENUM:
        case Vala.TokenType.ENSURES:
        case Vala.TokenType.ERRORDOMAIN:
        case Vala.TokenType.EXTERN:
        case Vala.TokenType.FALSE:
        case Vala.TokenType.FINALLY:
        case Vala.TokenType.FOR:
        case Vala.TokenType.FOREACH:
        case Vala.TokenType.GET:
        case Vala.TokenType.IF:
        case Vala.TokenType.IN:
        case Vala.TokenType.INLINE:
        case Vala.TokenType.INTERFACE:
        case Vala.TokenType.INTERNAL:
        case Vala.TokenType.IS:
        case Vala.TokenType.LOCK:
        case Vala.TokenType.NAMESPACE:
        case Vala.TokenType.NEW:
        case Vala.TokenType.NULL:
        case Vala.TokenType.OUT:
        case Vala.TokenType.OVERRIDE:
        case Vala.TokenType.OWNED:
        case Vala.TokenType.PARAMS:
        case Vala.TokenType.PRIVATE:
        case Vala.TokenType.PROTECTED:
        case Vala.TokenType.PUBLIC:
        case Vala.TokenType.REF:
        case Vala.TokenType.REQUIRES:
        case Vala.TokenType.RETURN:
        case Vala.TokenType.SEALED:
        case Vala.TokenType.SET:
        case Vala.TokenType.SIGNAL:
        case Vala.TokenType.SIZEOF:
        case Vala.TokenType.STATIC:
        case Vala.TokenType.STRUCT:
        case Vala.TokenType.SWITCH:
        case Vala.TokenType.THIS:
        case Vala.TokenType.THROW:
        case Vala.TokenType.THROWS:
        case Vala.TokenType.TRUE:
        case Vala.TokenType.TRY:
        case Vala.TokenType.TYPEOF:
#if VALA_0_42
        // FIXME: Valadoc ignores conditional
        //case Vala.TokenType.UNLOCK:
#endif
        case Vala.TokenType.UNOWNED:
        case Vala.TokenType.USING:
        case Vala.TokenType.VAR:
        case Vala.TokenType.VIRTUAL:
        case Vala.TokenType.VOID:
        case Vala.TokenType.VOLATILE:
        case Vala.TokenType.WEAK:
        case Vala.TokenType.WHILE:
        case Vala.TokenType.YIELD:
          var k = new GKeyword (n, loc);
          list.add (k);
          break;
        case Vala.TokenType.IDENTIFIER:
          string l = lines[loc.start.line -1];
          n = l.substring (loc.start.character - 1, loc.end.character - loc.start.character + 1);
          Symbol sym = null;
          if (server.root_server != null) {
            sym = server.root_server.get_symbol (n);
          } else {
            sym = server.get_symbol (n);
          }
          if (sym != null) {
            var ns = new GDocumentSymbol (sym, loc);
            list.add (ns);
          }
          break;
        case Vala.TokenType.STRING_LITERAL:
          var ns = new GStringLiteral (loc);
          var ds = new GDocumentSymbol (ns, loc);
          list.add (ds);
          break;
        // literals and signs
        case Vala.TokenType.INTEGER_LITERAL:
        case Vala.TokenType.REAL_LITERAL:
        case Vala.TokenType.CHARACTER_LITERAL:
        case Vala.TokenType.REGEX_LITERAL:
        case Vala.TokenType.TEMPLATE_STRING_LITERAL:
        case Vala.TokenType.VERBATIM_STRING_LITERAL:
        case Vala.TokenType.ASSIGN:
        case Vala.TokenType.ASSIGN_ADD:
        case Vala.TokenType.ASSIGN_BITWISE_AND:
        case Vala.TokenType.ASSIGN_BITWISE_OR:
        case Vala.TokenType.ASSIGN_BITWISE_XOR:
        case Vala.TokenType.ASSIGN_DIV:
        case Vala.TokenType.ASSIGN_MUL:
        case Vala.TokenType.ASSIGN_PERCENT:
        case Vala.TokenType.ASSIGN_SHIFT_LEFT:
        case Vala.TokenType.ASSIGN_SUB:
        case Vala.TokenType.BITWISE_AND:
        case Vala.TokenType.BITWISE_OR:
        case Vala.TokenType.DIV:
        case Vala.TokenType.MINUS:
        case Vala.TokenType.OP_AND:
        case Vala.TokenType.OP_COALESCING:
        case Vala.TokenType.OP_DEC:
        case Vala.TokenType.OP_EQ:
        case Vala.TokenType.OP_GE:
        case Vala.TokenType.OP_GT:
        case Vala.TokenType.OP_INC:
        case Vala.TokenType.OP_LE:
        case Vala.TokenType.OP_LT:
        case Vala.TokenType.OP_NE:
        case Vala.TokenType.OP_NEG:
        case Vala.TokenType.OP_OR:
        case Vala.TokenType.OP_PTR:
        case Vala.TokenType.OP_SHIFT_LEFT:
        // Carret?
        case Vala.TokenType.CARRET:
        // braces
        case Vala.TokenType.CLOSE_BRACE:
        case Vala.TokenType.CLOSE_BRACKET:
        case Vala.TokenType.CLOSE_PARENS:
        case Vala.TokenType.CLOSE_REGEX_LITERAL:
        case Vala.TokenType.CLOSE_TEMPLATE:
        case Vala.TokenType.OPEN_BRACE:
        case Vala.TokenType.OPEN_BRACKET:
        case Vala.TokenType.OPEN_PARENS:
        case Vala.TokenType.OPEN_REGEX_LITERAL:
        case Vala.TokenType.OPEN_TEMPLATE:
        //
        case Vala.TokenType.PERCENT:
        case Vala.TokenType.PLUS:
        case Vala.TokenType.SEMICOLON:
        case Vala.TokenType.STAR:
        case Vala.TokenType.TILDE:
        case Vala.TokenType.COLON:
        case Vala.TokenType.COMMA:
        case Vala.TokenType.DOUBLE_COLON:
        case Vala.TokenType.DOT:
        case Vala.TokenType.ELLIPSIS:
        case Vala.TokenType.INTERR:
        // Hash
        case Vala.TokenType.HASH:
          break;
      }
    }
    return list;
  }
}

public errordomain GVls.ScannerError {
  FILE_TYPE_ERROR,
  CONTENT_ERROR
}
