/* gvls-server-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.ServerCapabilities : Object {
  /**
   * Defines how text documents are synced. Is either a detailed structure defining each notification or
   * for backwards compatibility the TextDocumentSyncKind number. If omitted it defaults to `TextDocumentSyncKind.None`.
   */
  [CCode (nick="textDocumentSync")]
  public abstract TextDocumentSyncOptions text_document_sync { get; set; }
  /**
   * The server provides hover support.
   */
  [CCode (nick="hoverProvider")]
  public abstract bool hover_provider { get; set; }
  /**
   * The server provides completion support.
   */
  [CCode (nick="completionProvider")]
  public abstract CompletionOptions completion_rovider { get; set; }
  /**
   * The server provides signature help support.
   */
  [CCode (nick="signatureHelpProvider")]
  public abstract SignatureHelpOptions signature_help_provider { get; set; }
  /**
   * The server provides goto definition support.
   */
  [CCode (nick="definitionProvider")]
  public abstract bool definition_provider { get; set; }
  /**
   * The server provides Goto Type Definition support.
   *
   * Can be boolean | (TextDocumentRegistrationOptions & StaticRegistrationOptions)
   * Since 3.6.0
   */
  [CCode (nick="typeDefinitionProvider")]
  public abstract BooleanObject type_definition_provider { get; set; }
  /**
   * The server provides Goto Implementation support.
   *
   * Can be boolean | (TextDocumentRegistrationOptions & StaticRegistrationOptions)
   *
   * Since 3.6.0
   */
  [CCode (nick="implementationProvider")]
  public abstract BooleanObject implementation_provider { get; set; }
  /**
   * The server provides find references support.
   */
  [CCode (nick="referencesProvider")]
  public abstract bool references_provider { get; set; }
  /**
   * The server provides document highlight support.
   */
  [CCode (nick="documentHighlightProvider")]
  public abstract bool document_highlight_provider { get; set; }
  /**
   * The server provides document symbol support.
   */
  [CCode (nick="documentSymbolProvider")]
  public abstract bool document_symbol_provider { get; set; }
  /**
   * The server provides workspace symbol support.
   */
  [CCode (nick="workspaceSymbolProvider")]
  public abstract bool workspace_symbol_provider { get; set; }
  /**
   * The server provides code actions. The `CodeActionOptions` return type is only
   * valid if the client signals code action literal support via the property
   * `textDocument.codeAction.codeActionLiteralSupport`.
   *
   * Can be  boolean | CodeActionOptions
   */
  [CCode (nick="codeActionProvider")]
  public abstract BooleanObject code_action_provider { get; set; }
  /**
   * The server provides code lens.
   */
  [CCode (nick="codeLensProvider")]
  public abstract CodeLensOptions code_lens_provider { get; set; }
  /**
   * The server provides document formatting.
   */
  [CCode (nick="documentFormattingProvider")]
  public abstract bool document_formatting_provider { get; set; }
  /**
   * The server provides document range formatting.
   */
  [CCode (nick="documentRangeFormattingProvider")]
  public abstract bool document_range_formatting_provider { get; set; }
  /**
   * The server provides document formatting on typing.
   */
  [CCode (nick="documentOnTypeFormattingProvider")]
  public abstract DocumentOnTypeFormattingOptions documen_on_type_formatting_provider { get; set; }
  /**
   * The server provides rename support. RenameOptions may only be
   * specified if the client states that it supports
   * `prepareSupport` in its initial `initialize` request.
   *
   * Can be boolean | RenameOptions
   */
  [CCode (nick="renameProvider")]
  public abstract BooleanObject rename_provider { get; set; }
  /**
   * The server provides document link support.
   */
  [CCode (nick="documentLinkProvider")]
  public abstract DocumentLinkOptions document_link_provider { get; set; }
  /**
   * The server provides color provider support.
   *
   * Can be boolean | ColorProviderOptions | (ColorProviderOptions & TextDocumentRegistrationOptions & StaticRegistrationOptions)
   *
   * Since 3.6.0
   */
  [CCode (nick="colorProvider")]
  public abstract BooleanObject color_provider { get; set; }
  /**
   * The server provides folding provider support.
   *
   * Can be boolean | FoldingRangeProviderOptions | (FoldingRangeProviderOptions & TextDocumentRegistrationOptions & StaticRegistrationOptions)
   *
   * Since 3.10.0
   */
  [CCode (nick="foldingRangeProvider")]
  public abstract BooleanObject folding_range_provider { get; set; }
  /**
   * The server provides execute command support.
   */
  [CCode (nick="executeCommandProvider")]
  public abstract ExecuteCommandOptions execute_command_provider { get; set; }
  /**
   * Workspace specific server capabilities
   */
  [CCode (nick="workspace")]
  public abstract Workspace workspace { get; set; }
  public class Workspace : Object {
    /**
     * The server supports workspace folder.
     *
     * Since 3.6.0
     */
    [CCode (nick="workspaceFolders")]
    public WorkspaceFolders workspace_folders { get; set; }
    public class WorkspaceFolders : Object {
      /**
      * The server has support for workspace folders
      */
      [CCode (nick="supported")]
      public bool supported { get; set; }
      /**
      * Whether the server wants to receive workspace folder
      * change notifications.
      *
      * If a strings is provided the string is treated as a ID
      * under which the notification is registered on the client
      * side. The ID can be used to unregister for these events
      * using the `client/unregisterCapability` request.
      *
      * Can be string as a {@link StringObject} | boolean
      */
      [CCode (nick="changeNotifications")]
      public BooleanObject change_notifications { get; set; }
    }
  }
  /**
   * Experimental server capabilities.
   */
  [CCode (nick="experimental")]
  public abstract Object? experimental { get; set; }
}


/**
 * Completion options.
 */
public interface GVls.CompletionOptions : Object {
  /**
   * The server provides support to resolve additional
   * information for a completion item.
   */
  [CCode (nick="resolveProvider")]
  public abstract bool resolve_provider { get; set; }

  /**
   * The characters that trigger completion automatically.
   *
   * Container holds items of type {@link StringObject}
   */
  [CCode (nick="triggerCharacters")]
  public abstract Container trigger_characters { get; set; }
}

/**
 * Signature help options.
 */
public interface GVls.SignatureHelpOptions : Object {
  /**
   * The characters that trigger signature help
   * automatically.
   *
   * Container holding items of type {@link StringObject}
   */
  [CCode (nick="triggerCharacters")]
  public abstract Container trigger_characters { get; set; }
}

/**
 * Code Lens options.
 */
public interface GVls.CodeLensOptions : Object {
  /**
   * Code lens has a resolve provider as well.
   */
  [CCode (nick="resolveProvider")]
  public abstract bool resolve_provider { get; set; }
}

/**
 * Format document on type options.
 */
public interface GVls.DocumentOnTypeFormattingOptions : Object {
  /**
   * A character on which formatting should be triggered, like `}`.
   */
  [CCode (nick="firstTriggerCharacter")]
  public abstract string firstTriggerCharacter { get; set; }

  /**
   * More trigger characters.
   *
   * Container holding items of type {@link StringObject}
   */
  [CCode (nick="moreTriggerCharacter")]
  public abstract Container more_trigger_character { get; set; }
}

/**
 * Rename options
 */
public interface GVls.RenameOptions : Object {
	/**
	 * Renames should be checked and tested before being executed.
	 */
  [CCode (nick="prepareProvider")]
	 public abstract bool prepare_provider { get; set; }
}

/**
 * Document link options.
 */
public interface GVls.DocumentLinkOptions : Object {
  /**
   * Document links have a resolve provider as well.
   */
  [CCode (nick="resolveProvider")]
  public abstract bool resolve_provider { get; set; }
}

/**
 * Execute command options.
 */
public interface GVls.ExecuteCommandOptions : Object {
  /**
   * The commands to be executed on the server
   *
   * Container holding items of type {@link StringObject}
   */
  [CCode (nick="commands")]
  public abstract Container commands { get; set; }
}

/**
 * Save options.
 */
public interface GVls.SaveOptions : Object {
  /**
   * The client is supposed to include the content on save.
   */
  [CCode (nick="includeText")]
  public abstract bool include_text { get; set; }
}

/**
 * Color provider options.
 */
public interface GVls.ColorProviderOptions : Object {
}

/**
 * Folding range provider options.
 */
public interface GVls.FoldingRangeProviderOptions : Object {
}

public interface GVls.TextDocumentSyncOptions : Object {
	/**
	 * Open and close notifications are sent to the server.
	 */
  [CCode (nick="openClose")]
  public abstract bool open_close { get; set; }
	/**
	 * Change notifications are sent to the server. See TextDocumentSyncKind.None, TextDocumentSyncKind.Full
	 * and TextDocumentSyncKind.Incremental. If omitted it defaults to TextDocumentSyncKind.None.
	 */
  [CCode (nick="change")]
  public abstract int change { get; set; }
	/**
	 * Will save notifications are sent to the server.
	 */
  [CCode (nick="willSave")]
  public abstract bool will_save { get; set; }
	/**
	 * Will save wait until requests are sent to the server.
	 */
  [CCode (nick="willSaveWaitUntil")]
  public abstract bool will_save_wait_until { get; set; }
	/**
	 * Save notifications are sent to the server.
	 */
  [CCode (nick="save")]
  public abstract SaveOptions save { get; set; }
}

/**
 * Static registration options to be returned in the initialize request.
 */
public interface GVls.StaticRegistrationOptions : Object {
	/**
	 * The id used to register the request. The id can be used to deregister
	 * the request again. See also Registration#id.
	 */
  [CCode (nick="id")]
  public abstract string? id { get; set; }
}

