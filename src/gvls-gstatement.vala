/* gvls-gstatement.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Vala;

public class GVls.GStatement : Object,
                              GLib.ListModel,
                              Container,
                              DocumentSymbol,
                              GVls.Symbol,
                              GVls.Statement
{
  protected string _name = "@STATEMENT@";
  protected SymbolKind _kind = SymbolKind.KEY;
  protected Vala.Statement _statement = null;
  protected Location _location = null;
  protected Container _children = null;
  protected Package _package = null;
  protected Documentation _documentation = null;

  public GStatement.from_statement (Vala.Statement st, GLib.File file) {
    _statement = st;
    _location = new GLocation.from_values (file,
                                          st.source_reference.begin.line,
                                          st.source_reference.begin.column,
                                          st.source_reference.end.line,
                                          st.source_reference.end.column);
  }
  // ListModel
  public Object? get_item (uint position) {
    return null;
  }
  public Type get_item_type () { return typeof (Statement); }
  public uint get_n_items () {
    return 0;
  }
  public Object? get_object (uint position) { return get_item (position); }
  // Container
  public void add (Object object) {}
  public Object? find (Object key) { return null; }
  public void remove (Object object) {}
  // DocumentSymbol and Named Implementation
  public string name {
    get {
      return _name;
    }
  }

  public string? detail { get { return null; } }

  public SymbolKind kind { get { return _kind; } }

  public bool deprecated { get { return false; } }

  public Range range { get { return _location as Range; } }

  public Range selection_range { get { return _location as Range; } }

  public Container children { get { return _children; } }
  // Symbol
  public Location location { get { return _location; } }
  public Package package { get { return _package; } }
  public Documentation documentation { get { return _documentation; } }
  public SymbolKind symbol_kind { get { return symbol_kind; } }
  public string data_type { get { return "@STATEMENT@"; } }
  public Symbol? get_child (string name) { return null; }
  public void add_child (Symbol sym) {}
  public string full_name { owned get { return name.dup (); } }
}
