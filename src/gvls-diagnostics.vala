/* gvls-publish-diagnostics.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Diagnostics : Object {
  public signal void publish_diagnostics (GVls.RequestMessage msg);
  public virtual void report_diagnostics (GVls.RequestMessage msg, PublishDiagnosticsParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/publishDiagnostics";
    msg.@params.add (@params);
    publish_diagnostics (msg);
  }
}

public interface GVls.PublishDiagnosticsParams : Object {
  public abstract GLib.File file { get; set; }
	/**
	 * The URI for which diagnostic information is reported.
	 */
	public string uri { owned get { return file.get_uri (); } }

	/**
	 * An array of diagnostic information items of type {@link Diagnostic}
	 */
	public abstract Container diagnostics { get; }
}


public interface GVls.Diagnostic : Object {
  /**
   * The range at which the message applies.
   */
  public abstract Range range { get; set; }

  /**
   * The diagnostic's severity. Can be omitted. If omitted it is up to the
   * client to interpret diagnostics as error, warning, info or hint.
   */
  public abstract DiagnosticSeverity severity { get; set; }

  /**
   * The diagnostic's code, which might appear in the user interface.
   */
  public abstract string code { get; set; }

  /**
   * A human-readable string describing the source of this
   * diagnostic, e.g. 'typescript' or 'super lint'.
   */
  public abstract string source { get; set; }

  /**
   * The diagnostic's message.
   */
  public abstract string message { get; set; }

  /**
   * An array of related diagnostic information, e.g. when symbol-names within
   * a scope collide all definitions can be marked via this property.
   *
   * Container has items of type {@link DiagnosticRelatedInformation}
   */
  public abstract Container relatedInformation { get; }
}

public enum GVls.DiagnosticSeverity {
  UNKNOWN,
  /**
   * Reports an error.
   */
  ERROR,
  /**
   * Reports a warning.
   */
  WARNING,
  /**
   * Reports an information.
   */
  INFORMATION,
  /**
   * Reports a hint.
   */
  HINT;

  public string to_string () {
    string str = "";
    switch (this) {
      case ERROR:
        str = "Error";
        break;
      case WARNING:
        str = "Warning";
        break;
      case INFORMATION:
        str = "Information";
        break;
      case HINT:
        str = "Hint";
        break;
    }
    return str;
  }
}

/**
 * Represents a related message and source code location for a diagnostic. This should be
 * used to point to code locations that cause or related to a diagnostics, e.g when duplicating
 * a symbol in a scope.
 */
public interface GVls.DiagnosticRelatedInformation : Object {
  /**
   * The location of this related diagnostic information.
   */
  public abstract Location location { get; set; }

  /**
   * The message of this related diagnostic information.
   */
  public abstract string message { get; set; }
}

