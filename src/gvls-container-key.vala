/* gvls-container-key.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.ContainerKey : GLib.Object
{
  public virtual string? get_string () {
    if (this is ContainerKeyString)
      return (this as ContainerKeyString).key;
    return null;
  }
  public virtual int    get_integer () {
    if (this is ContainerKeyInteger)
      return (this as ContainerKeyInteger).key;
    return -1;
  }
  public virtual  Object get_object () { return this; }
}

public interface GVls.ContainerKeyString : ContainerKey
{
  public abstract string key { get; set; }
}

public interface GVls.ContainerKeyInteger : ContainerKey
{
  public abstract int key { get; set; }
}

public interface GVls.ContainerHashable : Object {
  public abstract string hash ();
  public abstract bool equal (Object obj);
}

