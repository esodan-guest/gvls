/* gvls-server-rpc.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;

public interface GVls.ServerLsp : Jsonrpc.Server
{
  public abstract Workspace workspace { get; internal set; }
  public abstract bool initialized { get; internal set; }
  public abstract GVls.Server server { get; internal set; }
  public virtual void run (IOStream stream)
    requires (workspace != null)
  {
    add_handler ("initialize", initialize);
    accept_io_stream (stream);
  }
  static void initialize (Jsonrpc.Server server, Jsonrpc.Client client,
                          string method, Variant id, Variant @params)
  {
    if (method != "initialize") return;
    var srv = server as ServerLsp;
    srv.initialized = true;
  }
}
