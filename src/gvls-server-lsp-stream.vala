/* gvls-server-lsp-stream.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;

public class GVls.ServerLspStream : Jsonrpc.Server, ServerLsp
{
  public Workspace workspace { get; internal set; }
  public bool initialized { get; internal set; }
  public GVls.Server server { get; internal set; }

  public ServerLspStream (IOStream stm) {
    server = new GServer ();
    workspace = new GWorkspace ();
  }
}
