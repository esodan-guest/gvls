/* gvls-test-document-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Text document specific client capabilities.
 */
public interface GVls.TextDocumentClientCapabilities : Object {

  [Description (nick="synchronization")]
  public abstract Synchronization synchronization { get; set; }
  public class Synchronization : Object {
    /**
     * Whether text document synchronization supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }

    /**
     * The client supports sending will save notifications.
     */
    [Description (nick="willSave")]
    public bool will_save { get; set; }

    /**
     * The client supports sending a will save request and
     * waits for a response providing text edits which will
     * be applied to the document before it is saved.
     */
    [Description (nick="willSaveWaitUntil")]
    public bool will_save_wait_until { get; set; }

    /**
     * The client supports did save notifications.
     */
    [Description (nick="didSave")]
    public bool did_save { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/completion`
   */
  [Description (nick="completion")]
  public abstract Completion completion  { get; set; }
  public class Completion : Object {
    /**
     * Whether completion supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }

    /**
     * The client supports the following `CompletionItem` specific
     * capabilities.
     */
    [Description (nick="completionItem")]
    public CompletionItem completion_item { get; set; }
    public class CompletionItem : Object {
      /**
       * The client supports snippets as insert text.
       *
       * A snippet can define tab stops and placeholders with `$1`, `$2`
       * and `${3:foo}`. `$0` defines the final tab stop, it defaults to
       * the end of the snippet. Placeholders with equal identifiers are linked,
       * that is typing in one will update others too.
       */
      [Description (nick="snippetSupport")]
      public bool snippet_support { get; set; }

      /**
       * The client supports commit characters on a completion item.
       */
      [Description (nick="commitCharactersSupport")]
      public bool commit_characters_support { get; set; }

      /**
       * The client supports the following content formats for the documentation
       * property. The order describes the preferred format of the client.
       *
       * Container holds items of type  {@link MarkupKindObject}
       */
      [Description (nick="documentationFormat")]
      public Container documentation_format { get; set; }

      /**
       * The client supports the deprecated property on a completion item.
       */
      [Description (nick="deprecatedSupport")]
      public bool deprecated_support { get; set; }

      /**
       * The client supports the preselect property on a completion item.
       */
      [Description (nick="preselectSupport")]
      public bool preselect_support { get; set; }
      
      construct {
        commit_characters_support = true;
      }
    }

    [Description (nick="completionItemKind")]
    public CompletionItemKind completion_item_kind { get; set; }
    public class CompletionItemKind : Object {
      /**
       * The completion item kind values the client supports. When this
       * property exists the client also guarantees that it will
       * handle values outside its set gracefully and falls back
       * to a default value when unknown.
       *
       * If this property is not present the client only supports
       * the completion items kinds from `Text` to `Reference` as defined in
       * the initial version of the protocol.
       *
       * Container holds items of type {@link CompletionItemKindObject}
       */
      [Description (nick="valueSet")]
      public Container? value_set { get; set; }
      
      construct {
        value_set = new GContainer ();
        value_set.add (new CompletionItemKindObject (CompletionItemKind.TEXT));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.METHOD));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.FIELD));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.VARIABLE));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.CLASS));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.INTERFACE));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.PROPERTY));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.ENUM));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.KEYWORD));
        value_set.add (new CompletionItemKindObject (CompletionItemKind.STRUCT));
      }
    }

    /**
     * The client supports to send additional context information for a
     * `textDocument/completion` request.
     */
    [Description (nick="contextSupport")]
    public bool context_support { get; set; }
    
    construct {
      completion_item_kind = new CompletionItemKind ();
    }
  }

  /**
   * Capabilities specific to the `textDocument/hover`
   */
  [Description (nick="hover")]
  public abstract Hover hover { get; set; }
  public class Hover : Object {
    /**
     * Whether hover supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }

    /**
     * The client supports the follow content formats for the content
     * property. The order describes the preferred format of the client.
     *
     * Container holds objects of type {@link MarkupKindObject}
     */
    [Description (nick="contentFormat")]
    public Container content_format { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/signatureHelp`
   */
  [Description (nick="signatureHelp")]
  public abstract SignatureHelp signature_help { get; set; }
  public class SignatureHelp : Object {
      /**
       * Whether signature help supports dynamic registration.
       */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }

    /**
     * The client supports the following `SignatureInformation`
     * specific properties.
     */
    [Description (nick="signatureInformation")]
    public SignatureInformation signature_information { get; set; }
    public class SignatureInformation : Object {
      /**
       * The client supports the follow content formats for the documentation
       * property. The order describes the preferred format of the client.
     *
     * Container holds objects of type {@link MarkupKindObject}
       */
      [Description (nick="documentationFormat")]
      public Container documentation_format { get; set; }
    }
  }

  /**
   * Capabilities specific to the `textDocument/references`
   */
  [Description (nick="references")]
  public abstract References references { get; set; }
  public class References : Object {
    /**
     * Whether references supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/documentHighlight`
   */
  [Description (nick="documentHighlight")]
  public abstract DocumentHighlight document_highlight { get; set; }
  public class DocumentHighlight : Object {
    /**
     * Whether document highlight supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/documentSymbol`
   */
  [Description (nick="documentSymbol")]
  public abstract DocumentSymbol documentSymbol { get; set; }
  public class DocumentSymbol : Object  {
    /**
     * Whether document symbol supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }

    /**
     * Specific capabilities for the `SymbolKind`.
     */
    [Description (nick="symbolKind")]
    public SymbolKind symbol_kind { get; set; }
    public class SymbolKind : Object {
      /**
       * The symbol kind values the client supports. When this
       * property exists the client also guarantees that it will
       * handle values outside its set gracefully and falls back
       * to a default value when unknown.
       *
       * If this property is not present the client only supports
       * the symbol kinds from `File` to `Array` as defined in
       * the initial version of the protocol.
       *
       * Container holds items of type {@link GVls.SymbolKind}
       */
      [Description (nick="valueSet")]
      public Container value_set { get; set; }

      construct {
        value_set = new GContainer ();
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.NAMESPACE));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.CLASS));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.METHOD));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.FIELD));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.ENUM));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.INTERFACE));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.VARIABLE));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.STRING));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.STRUCT));
        value_set.add (new GVls.SymbolKindObject (GVls.SymbolKind.ENUM_MEMBER));
      }
    }

    /**
     * The client supports hierarchical document symbols.
     */
    [Description (nick="hierarchicalDocumentSymbolSupport")]
    public bool hierarchical_document_symbol_support { get; set; }
    
    construct {
      symbol_kind = new DocumentSymbol.SymbolKind ();
    
    }
  }

  /**
   * Capabilities specific to the `textDocument/formatting`
   */
  [Description (nick="formatting")]
  public abstract Formating formatting { get; set; }
  public class Formating : Object {
    /**
     * Whether formatting supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/rangeFormatting`
   */
  [Description (nick="rangeFormatting")]
  public abstract RangeFormatting range_formatting { get; set; }
  public class RangeFormatting : Object {
    /**
     * Whether range formatting supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/onTypeFormatting`
   */
  [Description (nick="onTypeFormatting")]
  public abstract OnTypeFormatting on_type_formatting { get; set; }
  public class OnTypeFormatting : Object  {
    /**
     * Whether on type formatting supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/definition`
   */
  [Description (nick="definition")]
  public abstract Definition definition { get; set; }
  public class Definition : Object {
    /**
     * Whether definition supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/typeDefinition`
   *
   * Since 3.6.0
   */
  [Description (nick="typeDefinition")]
  public abstract TypeDefinition type_definition { get; set; }
  public class TypeDefinition : Object {
    /**
     * Whether typeDefinition supports dynamic registration. If this is set to `true`
     * the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)`
     * return value for the corresponding server capability as well.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamicRegistration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/implementation`.
   *
   * Since 3.6.0
   */
  [Description (nick="implementation")]
  public abstract Implementation implementation { get; set; }
  public class Implementation : Object {
    /**
     * Whether implementation supports dynamic registration. If this is set to `true`
     * the client supports the new `(TextDocumentRegistrationOptions & StaticRegistrationOptions)`
     * return value for the corresponding server capability as well.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/codeAction`
   */
  [Description (nick="codeAction")]
  public abstract CodeAction codeAction { get; set; }
  public class CodeAction : Object {
    /**
     * Whether code action supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
    /**
     * The client support code action literals as a valid
     * response of the `textDocument/codeAction` request.
     *
     * Since 3.8.0
     */
    [Description (nick="codeActionLiteralSupport")]
    public CodeActionLiteralSupport code_action_literal_support { get; set; }
    public class CodeActionLiteralSupport : Object {
      /**
       * The code action kind is support with the following value
       * set.
       */
      [Description (nick="codeActionKind")]
      public CodeActionKind code_action_kind { get; set; }
      public class CodeActionKind : Object {

        /**
         * The code action kind values the client supports. When this
         * property exists the client also guarantees that it will
         * handle values outside its set gracefully and falls back
         * to a default value when unknown.
         *
         * Container holds items of type {@link CodeActionKindObject}
         */
        [Description (nick="valueSet")]
        public Container value_set { get; set; }
      }
    }
  }

  /**
   * Capabilities specific to the `textDocument/codeLens`
   */
  [Description (nick="codeLens")]
  public abstract CodeLens code_lens { get; set; }
  public class CodeLens : Object {
    /**
     * Whether code lens supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/documentLink`
   */
  [Description (nick="documentLink")]
  public abstract DocumentLink document_link { get; set; }
  public class DocumentLink : Object {
    /**
     * Whether document link supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/documentColor` and the
   * `textDocument/colorPresentation` request.
   *
   * Since 3.6.0
   */
  [Description (nick="colorProvider")]
  public abstract ColorProvider colorProvider { get; set; }
  public class ColorProvider : Object {
    /**
     * Whether colorProvider supports dynamic registration. If this is set to `true`
     * the client supports the new `(ColorProviderOptions & TextDocumentRegistrationOptions & StaticRegistrationOptions)`
     * return value for the corresponding server capability as well.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `textDocument/rename`
   */
  [Description (nick="rename")]
  public abstract Rename rename { get; set; }
  public class Rename : Object {
    /**
     * Whether rename supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
    /**
     * The client supports testing for validity of rename operations
     * before execution.
     */
    [Description (nick="prepareSupport")]
    public bool prepare_support { get; set; }
  }

  /**
   * Capabilities specific to `textDocument/publishDiagnostics`.
   */
  [Description (nick="publishDiagnostics")]
  public abstract PublishDiagnostics publish_diagnostics { get; set; }
  public class PublishDiagnostics : Object {
    /**
     * Whether the clients accepts diagnostics with related information.
     */
    [Description (nick="relatedInformation")]
    public bool related_information { get; set; }
  }
  /**
   * Capabilities specific to `textDocument/foldingRange` requests.
   *
   * Since 3.10.0
   */
  [Description (nick="foldingRange")]
  public abstract FoldingRange foldingRange { get; set; }
  public class FoldingRange : Object {
	  /**
	   * Whether implementation supports dynamic registration for folding
	   * range providers. If this is set to `true`
     * the client supports the new
     * `(FoldingRangeProviderOptions & TextDocumentRegistrationOptions & StaticRegistrationOptions)`
     * return value for the corresponding server capability as well.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
    /**
     * The maximum number of folding ranges that the client prefers
     * to receive per document. The value serves as a
     * hint, servers are free to follow the limit.
     */
    [Description (nick="rangeLimit")]
    public int range_limit { get; set; }
    /**
     * If set, the client signals that it only supports folding
     * complete lines. If set, client will
     * ignore specified `startCharacter` and 
     *`endCharacter` properties in a FoldingRange.
     */
    [Description (nick="lineFoldingOnly")]
    public bool line_folding_only { get; set; }
  }
}

