/* gvls-text-document-open.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.DidChangeTextDocumentParams : Object {
  /**
   * The document that did change. The version number points
   * to the version after all provided content changes have
   * been applied.
   */
  public abstract VersionedTextDocumentIdentifier textDocument { get; set; }

  /**
   * The actual content changes. The content changes describe single state changes
   * to the document. So if there are two content changes c1 and c2 for a document
   * in state S then c1 move the document to S' and c2 to S ' '
   *
   * Container elements are of type {@link TextDocumentContentChangeEvent}
   */
  public abstract Container content_changes { get; }
}

/**
 * An event describing a change to a text document. If range and rangeLength are omitted
 * the new text is considered to be the full content of the document.
 */
public interface GVls.TextDocumentContentChangeEvent {
  /**
   * The range of the document that changed.
   */
  public abstract Range? range { get; set; }

  /**
   * The length of the range that got replaced.
   */
  public abstract int rangeLength { get; }

  /**
   * The new text of the range/document.
   */
  public abstract string text  { get; set; }
}

public interface GVls.DocumentFilter : Object {
  /**
   * A language id, like `typescript`.
   */
  public abstract string? language { get; set; }

  /**
   * A Uri [scheme](#Uri.scheme), like `file` or `untitled`.
   */
  public abstract string? scheme { get; set; }

  /**
   * A glob pattern, like `*.{ts,js}`.
   */
  public abstract string? pattern { get; set; }
}

/**
 * A container of {@link DocumentFilter} objects
 */
public interface GVls.DocumentSelector : Object, Container {}

public interface GVls.TextDocumentRegistrationOptions : Object {
  /**
   * A document selector to identify the scope of the registration. If set to null
   * the document selector provided on the client side will be used.
   */
  public abstract DocumentSelector? documentSelector { get; set; }
}

/**
 * Describe options to be used when registering for text document change events.
 */
public interface GVls.TextDocumentChangeRegistrationOptions : Object, TextDocumentRegistrationOptions {
	/**
	 * How documents are synced to the server. See TextDocumentSyncKind.Full
	 * and TextDocumentSyncKind.Incremental.
	 */
	public abstract TextDocumentSyncKind sync_kind { get; set; }
}

public interface GVls.TextDocumentIdentifier : Object {
  /**
   * The text document's URI.
   */
  public abstract string uri { get; set; }
}

public interface GVls.VersionedTextDocumentIdentifier : Object, TextDocumentIdentifier {
  /**
   * The version number of this document. If a versioned text document identifier
   * is sent from the server to the client and the file is not open in the editor
   * (the server has not received an open notification before) the server can send
   * `null` to indicate that the version is known and the content on disk is the
   * truth (as speced with document content ownership).
   *
   * The version number of a document will increase after each change, including
   * undo/redo. The number doesn't need to be consecutive.
   */
  public abstract int version { get; set; }
}


/**
 * Defines how the host (editor) should sync document changes to the language server.
 */
public enum GVls.TextDocumentSyncKind {
  /**
   * Documents should not be synced at all.
   */
  NONE,

  /**
   * Documents are synced by always sending the full content
   * of the document.
   */
  FULL,

  /**
   * Documents are synced by sending the full content on open.
   * After that only incremental updates to the document are
   * send.
   */
  INCREMENTAL;
  public string to_string () {
    string str = "";
    switch (this) {
      case NONE:
        str = "None";
        break;
      case FULL:
        str = "Full";
        break;
      case INCREMENTAL:
        str = "Incremental";
        break;
    }
    return str;
  }
}


public class GVls.TextDocumentSyncKindObject : GVls.EnumObject {
  construct {
    enum_type = typeof (TextDocumentSyncKind);
  }
  public TextDocumentSyncKindObject (TextDocumentSyncKind kind) {
    val = kind;
  }
  public TextDocumentSyncKind get_enum () { return (TextDocumentSyncKind) val; }
  public override string to_string () { return ((TextDocumentSyncKind) val).to_string (); }
}



