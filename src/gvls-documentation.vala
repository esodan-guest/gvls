/* gvls-documentation.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Documentation : GLib.Object
{
  public abstract string raw_doc { owned get; }
  public virtual string doc () {
    string d = "<p>";
    bool popen = true;
    try {
      InputStream istream = new MemoryInputStream.from_data ((uint8[]) raw_doc.dup ());
      DataInputStream di = new DataInputStream (istream);
      string l = "";
      bool paragraph = true;
      bool empty = false;
      bool first_line = false;
      while (true) {
        l = di.read_line ();
        if (l == null) {
          if (d.length == 0) {
            d = "<p>";
            popen = true;
          }
          if (popen)
            d = "</p>";
          break;
        }
        l = l.replace ("*", "");
        l = l.strip ();
        if (l.length == 0) {
          empty = true;
          paragraph = false;
        } else {
          paragraph = true;
          empty = false;
          if (!popen) {
            d += "<p>";
            popen = true;
            first_line = true;
          }
        }
        if (paragraph && l.length != 0) {
          if (!first_line) {
            d += " ";
          } else {
            first_line = false;
          }
          d += l;
        } else {
          if (popen) {
            d += "</p>";
            popen = false;
          }
        }
      }

    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
    return d;
  }
}
