/* gvls-completion.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Completion : Object {
  public signal void completion_request (GVls.RequestMessage msg);
  public signal void completion_item_request (GVls.RequestMessage msg);
  public signal void completion_hover_request (GVls.RequestMessage msg);
  public signal void completion_callable_signature_request (GVls.RequestMessage msg);
  public signal void completion_goto_request (GVls.RequestMessage msg);
  public signal void completion_goto_type_request (GVls.RequestMessage msg);
  public signal void completion_goto_implementation_request (GVls.RequestMessage msg);
  public signal void completion_find_references_request (GVls.RequestMessage msg);
  public signal void text_document_symbols_request (GVls.RequestMessage msg);

  public virtual void report_completion (GVls.RequestMessage msg, CompletionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/completion";
    msg.@params.add (@params);
    completion_request (msg);
  }
  public virtual void report_completion_item_resolve (GVls.RequestMessage msg, CompletionItem @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/resolve";
    msg.@params.add (@params);
    completion_item_request (msg);
  }
  public virtual void report_completion_hover (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/hover";
    msg.@params.add (@params);
    completion_hover_request (msg);
  }
  public virtual void report_completion_callable_signature (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/signatureHelp";
    msg.@params.add (@params);
    completion_callable_signature_request (msg);
  }
  public virtual void report_completion_goto (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/definition";
    msg.@params.add (@params);
    completion_goto_request (msg);
  }
  public virtual void report_completion_goto_type (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/typeDefinition";
    msg.@params.add (@params);
    completion_goto_type_request (msg);
  }
  public virtual void report_completion_goto_implementation (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/implementation";
    msg.@params.add (@params);
    completion_goto_implementation_request (msg);
  }
  public virtual void report_completion_find_references (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/references";
    msg.@params.add (@params);
    completion_find_references_request (msg);
  }
  public virtual void report_document_symbols (GVls.RequestMessage msg, DocumentSymbolParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/documentSymbol";
    msg.@params.add (@params);
    text_document_symbols_request (msg);
  }
}

public interface GVls.CompletionParams : Object, TextDocumentPositionParams {
  /**
   * The completion context. This is only available if the client specifies
   * to send this using `ClientCapabilities.textDocument.completion.contextSupport === true`
   */
  public abstract CompletionContext context { get; set; }
}

/**
 * How a completion was triggered
 */
[Flags]
public enum GVls.CompletionTriggerKind {
  UNKNOWN,
  /**
   * Completion was triggered by typing an identifier (24x7 code
   * complete), manual invocation (e.g Ctrl+Space) or via API.
   */
  INVOKED,

  /**
   * Completion was triggered by a trigger character specified by
   * the `triggerCharacters` properties of the `CompletionRegistrationOptions`.
   */
  TRIGGER_CHARACTER,

  /**
   * Completion was re-triggered as the current completion list is incomplete.
   */
  TRIGGER_FOR_INCOMPLETE_COMPLETIONS;

  public string to_string () {
    string str = "";
    var a = new Gee.ArrayList<string> ();
    if (INVOKED in this) {
      a.add ("Invoked");
    }
    if (TRIGGER_CHARACTER in this) {
      a.add ("TriggerCharacter");
    }
    if (TRIGGER_FOR_INCOMPLETE_COMPLETIONS in this) {
      a.add ("TriggerForIncompleteCompletions");
    }
    for (int i = 0; i < a.size; i++) {
      str += a.get (i);
      if (i+1 < a.size) {
        str += "|";
      }
    }
    return str;
  }
}


/**
 * Contains additional information about the context in which a completion request is triggered.
 */
public interface GVls.CompletionContext : Object {
  /**
   * How the completion was triggered.
   */
  public abstract CompletionTriggerKind trigger_kind { get; set; }

  /**
   * The trigger character (a single character) that has trigger code complete.
   * Is undefined if `triggerKind !== CompletionTriggerKind.TriggerCharacter`
   */
  public abstract string? trigger_character { get; set; }
}

public interface GVls.TextDocumentPositionParams : Object {
  /**
   * The text document.
   */
  public abstract TextDocumentIdentifier text_document { get; set; }

  /**
   * The position inside the text document.
   */
  public abstract Position position { get; set; }
}

/**
 * Represents a collection of [completion items](#CompletionItem) to be presented
 * in the editor.
 */
public interface GVls.CompletionList : Object {
  /**
   * This list it not complete. Further typing should result in recomputing
   * this list.
   */
  public abstract bool is_incomplete { get; set; }

  /**
   * The completion items of type {@link CompletionItem}
   */
  public abstract Container items { get; }
}

/**
 * Defines whether the insert text in a completion item should be interpreted as
 * plain text or a snippet.
 */
public enum GVls.InsertTextFormat {
  UNKNOWN,
  /**
   * The primary text to be inserted is treated as a plain string.
   */
  PLAIN_TEXT,

  /**
   * The primary text to be inserted is treated as a snippet.
   *
   * A snippet can define tab stops and placeholders with `$1`, `$2`
   * and `${3:foo}`. `$0` defines the final tab stop, it defaults to
   * the end of the snippet. Placeholders with equal identifiers are linked,
   * that is typing in one will update others too.
   */
  SNIPPED;

  public string to_string () {
    string str = "";
    switch (this) {
      case PLAIN_TEXT:
        str = "PlainText";
        break;
      case SNIPPED:
        str = "Snippet";
        break;
      default:
        str = "Unknown";
        break;
    }
    return str;
  }
}

public interface GVls.CompletionItem : Object {
  /**
   * The label of this completion item. By default
   * also the text that is inserted when selecting
   * this completion.
   */
  public abstract string label { get; set; }

  /**
   * The kind of this completion item. Based of the kind
   * an icon is chosen by the editor.
   */
  public abstract CompletionItemKind kind { get; set; }

  /**
   * A human-readable string with additional information
   * about this item, like type or symbol information.
   */
  public abstract string? detail { get; set; }

  /**
   * A human-readable string that represents a doc-comment.
   */
  public abstract MarkupContent? documentation { get; set; }

  /**
   * Indicates if this item is deprecated.
   */
  public abstract bool deprecated { get; set; }

  /**
   * Select this item when showing.
   *
   * *Note* that only one completion item can be selected and that the
   * tool / client decides which item that is. The rule is that the *first*
   * item of those that match best is selected.
   */
  public abstract bool preselect { get; set; }

  /**
   * A string that should be used when comparing this item
   * with other items. When `falsy` the label is used.
   */
  public abstract string? sort_text { get; set; }

  /**
   * A string that should be used when filtering a set of
   * completion items. When `falsy` the label is used.
   */
  public abstract string? filter_text { get; set; }

  /**
   * A string that should be inserted into a document when selecting
   * this completion. When `falsy` the label is used.
   *
   * The `insertText` is subject to interpretation by the client side.
   * Some tools might not take the string literally. For example
   * VS Code when code complete is requested in this example `con<cursor position>`
   * and a completion item with an `insertText` of `console` is provided it
   * will only insert `sole`. Therefore it is recommended to use `textEdit` instead
   * since it avoids additional client side interpretation.
   *
   * Deprecated Use textEdit instead.
   */
  [Version (deprecated=true)]
  public abstract string? insert_text { get; set; }

  /**
   * The format of the insert text. The format applies to both the `insertText` property
   * and the `newText` property of a provided `textEdit`.
   */
  public abstract InsertTextFormat? insert_text_format { get; set; }

  /**
   * An edit which is applied to a document when selecting this completion. When an edit is provided the value of
   * `insertText` is ignored.
   *
   * *Note:* The range of the edit must be a single line range and it must contain the position at which completion
   * has been requested.
   */
  public abstract TextEdit? text_edit { get; set; }

  /**
   * An optional array of additional text edits that are applied when
   * selecting this completion. Edits must not overlap (including the same insert position)
   * with the main edit nor with themselves.
   *
   * Additional text edits should be used to change text unrelated to the current cursor position
   * (for example adding an import statement at the top of the file if the completion item will
   * insert an unqualified type).
   *
   * Container's items are of type {@link TextEdit}
   */
  public abstract Container additional_text_edits { get; }

  /**
   * An optional set of characters that when pressed while this completion is active will accept it first and
   * then type that character. *Note* that all commit characters should have `length=1` and that superfluous
   * characters will be ignored.
   *
   * Container's items are of type {@link StringObject}
   */
  public abstract Container commit_characters { get; }

  /**
   * An optional command that is executed *after* inserting this completion. *Note* that
   * additional modifications to the current document should be described with the
   * additionalTextEdits-property.
   */
  public abstract Command? command { get; set; }

  /**
   * An data entry field that is preserved on a completion item between
   * a completion and a completion resolve request.
   */
  public abstract Object? data { get; set; }
}

/**
 * The kind of a completion entry.
 */
public enum GVls.CompletionItemKind {
  UNKNOWN,
  TEXT,
  METHOD,
  FUNCTION,
  CONSTRUCTOR,
  FIELD,
  VARIABLE,
  CLASS,
  INTERFACE,
  MODULE,
  PROPERTY,
  UNIT,
  VALUE,
  ENUM,
  KEYWORD,
  SNIPPED,
  COLOR,
  FILE,
  REFERENCE,
  FOLDER,
  ENUM_MEMBER,
  CONSTANT,
  STRUCT,
  EVENT,
  OPERATOR,
  TYPE_PARAMETER;

  public string to_string () {
    string str = "";
    switch (this) {
      case TEXT:
        str = "Text";
        break;
      case METHOD:
        str = "Method";
        break;
      case FUNCTION:
        str = "Function";
        break;
      case CONSTRUCTOR:
        str = "Constructor";
        break;
      case FIELD:
        str = "Field";
        break;
      case VARIABLE:
        str = "Variable";
        break;
      case CLASS:
        str = "Class";
        break;
      case INTERFACE:
        str = "Interface";
        break;
      case MODULE:
        str = "Module";
        break;
      case PROPERTY:
        str = "Property";
        break;
      case UNIT:
        str = "Unit";
        break;
      case VALUE:
        str = "Value";
        break;
      case ENUM:
        str = "Enum";
        break;
      case KEYWORD:
        str = "Keyword";
        break;
      case SNIPPED:
        str = "Snippet";
        break;
      case COLOR:
        str = "Color";
        break;
      case FILE:
        str = "File";
        break;
      case REFERENCE:
        str = "Reference";
        break;
      case FOLDER:
        str = "Folder";
        break;
      case ENUM_MEMBER:
        str = "EnumMember";
        break;
      case CONSTANT:
        str = "Constant";
        break;
      case STRUCT:
        str = "Struct";
        break;
      case EVENT:
        str = "Event";
        break;
      case OPERATOR:
        str = "Operator";
        break;
      case TYPE_PARAMETER:
        str = "TypeParameter";
        break;
    }
    return str;
  }
}

public interface GVls.CompletionRegistrationOptions : Object, TextDocumentRegistrationOptions {
  /**
   * Most tools trigger completion request automatically without explicitly requesting
   * it using a keyboard shortcut (e.g. Ctrl+Space). Typically they do so when the user
   * starts to type an identifier. For example if the user types `c` in a JavaScript file
   * code complete will automatically pop up present `console` besides others as a
   * completion item. Characters that make up identifiers don't need to be listed here.
   *
   * If code complete should automatically be trigger on characters not being valid inside
   * an identifier (for example `.` in JavaScript) list them in `triggerCharacters`.
   *
   * Container's items are of type {@link StringObject}
   */
  public abstract Container trigger_characters  { get; }

  /**
   * The server provides support to resolve additional
   * information for a completion item.
   */
  public abstract bool resolve_provider { get; set; }
}

/**
 * The result of a hover request.
 */
public interface GVls.Hover : Object {
  /**
   * The hover's content as a colection of {@link MarkedString}
   */
  public abstract Container contents { get; }

  /**
   * An optional range is a range inside a text document
   * that is used to visualize a hover, e.g. by changing the background color.
   */
  public abstract Range range { get; set; }
}

public class GVls.MarkedString : Object {
  public string language { get; set; }
  public string @value { get; set; }
}

/**
 * Signature help represents the signature of something
 * callable. There can be multiple signature but only one
 * active and only one active parameter.
 */
public interface GVls.SignatureHelp : Object {
  /**
   * One or more signatures of type {@link SignatureInformation}
   */
  public abstract Container signatures { get; }

  /**
   * The active signature. If omitted or the value lies outside the
   * range of `signatures` the value defaults to zero or is ignored if
   * `signatures.length === 0`. Whenever possible implementors should
   * make an active decision about the active signature and shouldn't
   * rely on a default value.
   * In future version of the protocol this property might become
   * mandatory to better express this.
   */
  public abstract int active_signature { get; set; }

  /**
   * The active parameter of the active signature. If omitted or the value
   * lies outside the range of `signatures[activeSignature].parameters`
   * defaults to 0 if the active signature has parameters. If
   * the active signature has no parameters it is ignored.
   * In future version of the protocol this property might become
   * mandatory to better express the active parameter if the
   * active signature does have any.
   */
  public abstract int active_parameter { get; set; }
}

/**
 * Represents the signature of something callable. A signature
 * can have a label, like a function-name, a doc-comment, and
 * a set of parameters.
 */
public interface GVls.SignatureInformation : Object {
  /**
   * The label of this signature. Will be shown in
   * the UI.
   */
  public abstract string label { get; set; }

  /**
   * The human-readable doc-comment of this signature. Will be shown
   * in the UI but can be omitted.
   */
  public abstract MarkupContent documentation { get; set; }

  /**
   * The parameters of this signature of the type {@link ParameterInformation}.
   */
  public abstract Container parameters { get; }
}

/**
 * Represents a parameter of a callable-signature. A parameter can
 * have a label and a doc-comment.
 */
public interface GVls.ParameterInformation : Object {
  /**
   * The label of this parameter. Will be shown in
   * the UI.
   */
  public abstract string label { get; set; }

  /**
   * The human-readable doc-comment of this parameter. Will be shown
   * in the UI but can be omitted.
   */
  public abstract MarkupContent documentation { get; }
}

public interface GVls.SignatureHelpRegistrationOptions : Object, TextDocumentRegistrationOptions {
  /**
   * The characters that trigger signature help
   * automatically.
   *
   * Container's item's type are {@link StringObject}
   */
  public abstract Container trigger_characters { get; }
}

/**
 * Describes the content type that a client supports in various
 * result literals like `Hover`, `ParameterInfo` or `CompletionItem`.
 *
 * Please note that `MarkupKinds` must not start with a `$`. This kinds
 * are reserved for internal usage.
 */
public enum GVls.MarkupKind {
  UNKNOWN,
  /**
   * Plain text is supported as a content format
   */
  PLAIN_TEXT,

  /**
   * Markdown is supported as a content format
   */
  MARKDOWN;

  public string to_string () {
    string str = "";
    switch (this) {
      case PLAIN_TEXT:
        str = "plaintext";
        break;
      case MARKDOWN:
        str = "markdown";
        break;
      default:
        str = "unknown";
        break;
    }
    return str;
  }
}


public class GVls.MarkupKindObject : GVls.EnumObject {
  construct {
    enum_type = typeof (MarkupKind);
  }
  public MarkupKindObject (MarkupKind kind) {
    val = kind;
  }
  public MarkupKind get_enum () { return (MarkupKind) val; }
  public override string to_string () { return ((MarkupKind) val).to_string (); }
}

/**
 * A `MarkupContent` literal represents a string value which content is interpreted base on its
 * kind flag. Currently the protocol supports `plaintext` and `markdown` as markup kinds.
 *
 * If the kind is `markdown` then the value can contain fenced code blocks like in GitHub issues.
 * See [[https://help.github.com/articles/creating-and-highlighting-code-blocks/#syntax-highlighting]]
 *
 * Here is an example how such a string can be constructed using JavaScript / TypeScript:
 * {{{ts
 * let markdown: MarkdownContent = {
 *  kind: MarkupKind.Markdown,
 *	value: [
 *		'# Header',
 *		'Some text',
 *		'```typescript',
 *		'someCode();',
 *		'```'
 *	].join('\n')
 * };
 * }}}
 *
 * *Please Note* that clients might sanitize the return markdown. A client could decide to
 * remove HTML from the markdown to avoid script execution.
 */
public interface GVls.MarkupContent : Object {
  /**
   * The type of the Markup from: {@link MarkupKind} namespace
   */
  public abstract MarkupKind kind { get; set; }

  /**
   * The content itself
   */
  public abstract string @value { get; set; }
}

public interface GVls.TextEdit : Object {
  /**
   * The range of the text document to be manipulated. To insert
   * text into a document create a range where start === end.
   */
  public abstract Range range { get; set; }

  /**
   * The string to be inserted. For delete operations use an
   * empty string.
   */
  public abstract string new_text { get; set; }
}

public interface GVls.Command : Object {
	/**
	 * Title of the command, like `save`.
	 */
	public abstract string title { get; set; }
	/**
	 * The identifier of the actual command handler.
	 */
	public abstract string command { get; set; }
	/**
	 * Arguments that the command handler should be
	 * invoked with.
	 *
	 * Container's members are of type {@link GLib.Object}
	 */
	public abstract Container arguments { get; }
}


/**
 * Represents programming constructs like variables, classes, interfaces etc. that appear in a document. Document symbols can be
 * hierarchical and they have two ranges: one that encloses its definition and one that points to its most interesting range,
 * e.g. the range of an identifier.
 */
public interface GVls.DocumentSymbol : Object {
	/**
	 * The name of this symbol.
	 */
	public abstract string name { get; }

	/**
	 * More detail for this symbol, e.g the signature of a function.
	 */
	public abstract string? detail { get; }

	/**
	 * The kind of this symbol.
	 */
	public abstract SymbolKind kind { get; }

	/**
	 * Indicates if this symbol is deprecated.
	 */
	public abstract bool deprecated { get; }

	/**
	 * The range enclosing this symbol not including leading/trailing whitespace but everything else
	 * like comments. This information is typically used to determine if the clients cursor is
	 * inside the symbol to reveal in the symbol in the UI.
	 */
	public abstract Range range { get; }

	/**
	 * The range that should be selected and revealed when this symbol is being picked, e.g the name of a function.
	 * Must be contained by the `range`.
	 */
	public abstract Range selection_range { get; }

	/**
	 * Children of this symbol, e.g. properties of a class.
	 *
	 * Container's items are of type {@link DocumentSymbol}
	 */
	public abstract Container children { get; }
}

/**
 * The kind of a completion entry.
 */
public enum CompletionItemKind {
  UNKNOWN,
  TEXT,
  METHOD,
  FUNCTION,
  CONSTRUCTOR,
  FIELD,
  VARIABLE,
  CLASS,
  INTERFACE,
  MODULE,
  PROPERTY,
  UNIT,
  VALUE,
  ENUM,
  KEYWORD,
  SNIPPET,
  COLOR,
  FILE,
  REFERENCE,
  FOLDER,
  ENUMMEMBER,
  CONSTANT,
  STRUCT,
  EVENT,
  OPERATOR,
  TYPEPARAMETER;
  public string to_string () {
    string str = "";
    switch (this) {
      case TEXT:
        str = "Text";
        break;
      case METHOD:
        str = "Method";
        break;
      case FUNCTION:
        str = "Function";
        break;
      case CONSTRUCTOR:
        str = "Constructor";
        break;
      case FIELD:
        str = "Field";
        break;
      case VARIABLE:
        str = "Variable";
        break;
      case CLASS:
        str = "Class";
        break;
      case INTERFACE:
        str = "Interface";
        break;
      case MODULE:
        str = "Module";
        break;
      case PROPERTY:
        str = "Property";
        break;
      case UNIT:
        str = "Unit";
        break;
      case VALUE:
        str = "Value";
        break;
      case ENUM:
        str = "Enum";
        break;
      case KEYWORD:
        str = "Keyword";
        break;
      case SNIPPET:
        str = "SNIPPET";
        break;
      case COLOR:
        str = "Color";
        break;
      case FILE:
        str = "File";
        break;
      case REFERENCE:
        str = "Reference";
        break;
      case FOLDER:
        str = "Folder";
        break;
      case ENUMMEMBER:
        str = "EnumMember";
        break;
      case CONSTANT:
        str = "Constant";
        break;
      case STRUCT:
        str = "Struct";
        break;
      case EVENT:
        str = "Event";
        break;
      case OPERATOR:
        str = "Operator";
        break;
      case TYPEPARAMETER:
        str = "TypeParameter";
        break;
      case UNKNOWN:
        str = "Unknown";
        break;
    }
    return str;
  }
}

public class GVls.CompletionItemKindObject : GVls.EnumObject {
  construct {
    enum_type = typeof (CompletionItemKind);
  }
  public CompletionItemKindObject (CompletionItemKind kind) {
    val = kind;
  }
  public CompletionItemKind get_enum () { return (CompletionItemKind) val; }
  public override string to_string () { return ((CompletionItemKind) val).to_string (); }
}
