/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gvls/parse/class",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        assert (server.symbols != null);
        assert (server.symbols is GContainer);
        assert (server.symbols is GVls.Container);
        assert (server.symbols is Gee.ArrayList);
        assert (server.symbols is GLib.ListModel);
        message ("Symbols found: %u", server.symbols.get_n_items ());
        for (int i = 0; i < server.symbols.get_n_items (); i++) {
          var sym = server.symbols.get_item (i) as Symbol;
          assert (sym != null);
          message ("Symbol found: %s", sym.name);
        }
        assert (server.symbols.get_n_items () == 5);
        var cl = server.get_symbol ("App");
        assert (cl != null);
        assert (cl.get_child ("name") != null);
        assert (cl.get_child ("method") != null);
        var cl2 = server.get_symbol ("App2");
        assert (cl2 != null);
        assert (cl2.get_child ("callme") != null);
        var iface = server.get_symbol ("IfaceApp");
        assert (iface != null);
        assert (iface.get_child ("callme") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/namespace",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        message ("Found: %u", server.symbols.get_n_items ());
        for (int i = 0; i < server.symbols.get_n_items (); i++) {
          var sym = server.symbols.get_item (i) as Symbol;
          assert (sym != null);
          message ("Symbol found: %s", sym.name);
        }
        assert (server.symbols.get_n_items () == 10);
        var ns = server.get_symbol ("MyOwn");
        assert (ns != null);
        assert (ns.get_child ("App") != null);
        assert (ns.get_child ("App2") != null);
        assert (ns.get_child ("IfaceApp") != null);
        var cl = server.get_symbol ("App");
        assert (cl != null);
        assert (cl.get_child ("name") != null);
        assert (cl.get_child ("method") != null);
        var cl2 = server.get_symbol ("App2");
        assert (cl2 != null);
        assert (cl2.get_child ("callme") != null);
        var iface = server.get_symbol ("IfaceApp");
        assert (iface != null);
        assert (iface.get_child ("callme") != null);
        var sbns = server.get_symbol ("SubMyOwn");
        assert (sbns != null);
        var clt = sbns.get_child ("Task");
        assert (clt != null);
        assert (clt.get_child ("time") != null);
        var tns = server.get_symbol ("MyTwo");
        assert (tns != null);
        var ic = tns.get_child ("IfaceCome");
        assert (ic != null);
        assert (ic.get_child ("total") != null);
        var nnscl = server.get_symbol ("Tycom");
        assert (nnscl != null);
        assert (nnscl.get_child ("rock") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/location",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var cl = server.get_symbol ("App");
        assert (cl != null);
        assert (cl.location != null);
        message (cl.location.to_string ());
        var loc = new GLocation.from_values (file, 5, 1, 5, 1);
        assert (cl.is_at (loc));
        var s = server.find_at (loc);
        assert (s != null);
        message (s.name);
        assert (s.name == "App");
        message (loc.to_string ());
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/multi",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file1 = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file1.query_exists ());
      GLib.File file2 = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file2.query_exists ());
      Server server = new GServer () as Server;
      Server server1 = new GServer () as Server;
      Server server2 = new GServer () as Server;
      try {
        server1.parse (file1);
        server2.parse (file2);
        assert (server2.get_symbol ("MyOwn") != null);
        assert (server2.get_symbol ("MyOwn.App") != null);
        assert (server2.get_symbol ("MyOwn.App.SubApp") != null);
        assert (server2.get_symbol ("MyOwn.App.SubApp.SubApps") != null);
        assert (server2.get_symbol ("MyOwn.App.SubApp.SubApps.ONE") != null);
        server.add_server (server1);
        message ("Server1 number of symbols: %u", server1.symbols.get_n_items ());
        server.add_server (server2);
        message ("Server2 number of symbols: %u", server2.symbols.get_n_items ());
        var cl = server.get_symbol ("App");
        assert (cl != null);
        message ("Class found: %s", cl.name);
        message ("Class's full name found: %s", cl.full_name);
        assert (cl.full_name == "App");
        assert (server.get_symbol ("MyOwn") != null);
        message ("Searching for: MyOwn.App");
        var cl2 = server.get_symbol ("MyOwn.App");
        assert (cl2 != null);
        message ("Class found: %s", cl2.name);
        message ("Class's full name found: '%s'", cl2.full_name);
        assert (cl2.full_name == "MyOwn.App");
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/vapi",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/test.vapi");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        Server server1 = server.create_pkg_server (file);
        message ("Server1 number of symbols: %u", server1.symbols.get_n_items ());
        assert (server1.symbols.get_n_items () != 0);
        assert (server1.get_symbol ("Test") != null);
        assert (server1.get_symbol ("Time") != null);
        assert (!(server.get_symbol ("Test") != null));
        assert (!(server.get_symbol ("Time") != null));
        server.add_server (server1);
        assert (server.get_symbol ("Test") != null);
        assert (server.get_symbol ("Time") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/find-pkg-file",
    ()=>{
      Server server = new GServer () as Server;
      try {
        var fpkg = server.find_package_file ("glib-2.0");
        assert (fpkg != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/find-namespace-file",
    ()=>{
      try {
        Server server = new GServer () as Server;
        var fpkg = server.find_namespace_file ("GLib");
        assert (fpkg != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/add-namespace",
    ()=>{
      try {
        var envp = GLib.Environ.get ();
        string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
        GLib.File dir = GLib.File.new_for_path (SRC_DIR);
        assert (dir.query_exists ());
        Server server = new GServer () as Server;
        server.add_vapi_dir (dir);
        assert (server.add_namespace ("Test"));
        assert (server.get_symbol ("Test") != null);
        assert (server.get_symbol ("Time") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/glib-namespace/package",
    ()=>{
      try {
        Server server = new GServer () as Server;
        server.add_default_vapi_dirs ();
        var glibf = server.find_package_file ("glib-2.0");
        assert (glibf != null);
        server.parse (glibf);
        assert (server.get_symbol ("string") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/glib-namespace/namespace",
    ()=>{
      try {
        Server server = new GServer () as Server;
        server.add_default_vapi_dirs ();
        var glibf = server.find_namespace_file ("GLib");
        assert (glibf != null);
        server.add_default_namespaces ();
        assert (server.servers.get_n_items () == 3);
        server.parse (glibf);
        assert (server.symbols.get_n_items () != 0);
        var root = server.get_symbol("@RootNamespace@");
        assert (root != null);
        // for (int i = 0; i < root.get_n_items (); i++) {
        //   message ("Root Symbol found: %s".printf ((root.get_item (i) as Symbol).name));
        // }
        // for (int i = 0; i < server.symbols.get_n_items (); i++) {
        //   message ("Symbol found: %s".printf ((server.symbols.get_item (i) as Symbol).name));
        // }
        assert (server.get_symbol ("string") != null);
        assert (server.get_symbol ("string.split") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/using/namespace/versioned",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var l = server.find_using_namespaces ();
        assert (l != null);
        assert (l.get_n_items () == 1);
        assert ((l.get_item (0) as StringObject).to_string () == "GLib");
        server.add_using_namespaces ();
        assert (server.get_symbol ("string") != null);
        assert (server.get_symbol ("string.replace") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/using/namespace-all",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file1 = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file1.query_exists ());
      GLib.File file2 = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file2.query_exists ());
      Server server = new GServer () as Server;
      Server server1 = new GServer () as Server;
      Server server2 = new GServer () as Server;
      try {
        server.add_server (server1);
        server.add_server (server2);
        server1.parse (file1);
        server2.parse (file2);
        message ("Servers: %u", server.servers.get_n_items ());
        assert (server.servers.get_n_items () == 6);
        assert (server.get_symbol ("GLib") != null);
        assert (server.get_symbol ("string") != null);
        assert (server.get_symbol ("string.replace") != null);
        assert (server.get_symbol ("Cairo") != null);
        assert (server.get_symbol ("Object") != null);
        assert (server.get_symbol ("InputStream") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/using/namespace/unversioned",
    ()=>{
      Server server = new GServer () as Server;
      try {
        server.add_default_vapi_dirs ();
        message ("VAPI dirs: %u", server.vapi_dirs.get_n_items ());
        assert (server.vapi_dirs.get_n_items () >= 1);
        server.add_namespace ("GLib");
        assert (server.get_symbol ("GLib") != null);
        server.add_namespace ("Gtk");
        assert (server.get_symbol ("Gtk") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/using/namespace/localdir",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file1 = GLib.File.new_for_path (SRC_DIR+"/test.vapi");
      GLib.File file2 = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file1.query_exists ());
      assert (file2.query_exists ());
      Server server = new GServer () as Server;
      Server server1 = new GServer () as Server;
      try {
        server.add_server (server1);
        server1.parse (file2);
        var ndirs = server.vapi_dirs.get_n_items ();
        message ("Local Dirs number: %u", ndirs);
        /* 3 if prefix is /usr, 2 in other cases */
        assert (ndirs == 3 || ndirs == 2);
        assert (server.add_namespace ("Test"));
        assert (server.get_symbol ("Test") != null);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/documentation",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var s1 = server.get_symbol ("App");
        assert (s1 != null);
        assert (s1.documentation != null);
        assert (s1.documentation.raw_doc != null);
        assert (s1.documentation.doc () != null);
        message (s1.documentation.doc ());
        assert (s1.documentation.doc () == "<p></p>");
        var s2 = server.get_symbol ("App2");
        assert (s2 != null);
        assert (s2.documentation != null);
        assert (s2.documentation.raw_doc != null);
        assert (s2.documentation.doc () != null);
        message (s2.documentation.doc ());
        assert (s2.documentation.doc () == """<p></p><p>Test documentation one line</p>""");
        var s3 = server.get_symbol ("App2.callme");
        assert (s3 != null);
        assert (s3.documentation != null);
        assert (s3.documentation.raw_doc != null);
        assert (s3.documentation.doc () != null);
        message (s3.documentation.doc ());
        assert (s3.documentation.doc () == """<p></p><p>Test documentation with multiple lines in the help text</p>""");
        var s4 = server.get_symbol ("IfaceApp");
        assert (s4 != null);
        assert (s4.documentation != null);
        assert (s4.documentation.raw_doc != null);
        assert (s4.documentation.doc () != null);
        message (s4.documentation.doc ());
        assert (s4.documentation.doc () == """<p></p><p>Test interface with documentation in place, multiple paragrap.</p><p>Second paragraph.</p>""");
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/parse/invalid",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/invalid.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var cl = server.get_symbol ("App");
        assert (cl != null);
        var m = server.get_symbol ("call");
        assert (m != null);
        assert (m is Symbol);
        assert (m.symbol_kind == SymbolKind.METHOD);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    return Test.run ();
  }
}
