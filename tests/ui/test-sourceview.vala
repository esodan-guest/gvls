/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvdaui Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GVlsui;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Gtk.init (ref args);
    Test.add_func ("/gvlsui/sourceview/init",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var win = new Gtkt.WindowTester ();
        var w = new GVlsui.SourceView ();
        assert (w.buffer != null);
        w.hexpand = true;
        w.set_wrap_mode (Gtk.WrapMode.WORD);
        w.set_auto_indent (true);
        w.set_indent_on_tab (true);
        win.waiting_for_event = TestConfig.INTERACTIVE;
        w.server = server;
        win.widget = w;
        win.add_test ("Parse pre-defined buffer", "A text on view should be parsed. Try to use a symbol");
        win.initialize.connect (()=>{
          message ("Setting Text to Buffer");
          w.buffer.text = """
public class Test {
  public void method () { return; }
  public string name { get; set; }
}
public interface Iface {
  public abstract void callme ();
  public abstract int time { get; }
}
public class App {
  public static void main () {
    Test t;
  }
}

""";
        });
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvlsui/sourceview/class-fields",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file.query_exists ());
      try {
        Server server = new GServer () as Server;
        var win = new Gtkt.WindowTester ();
        var w = new GVlsui.SourceView ();
        assert (w.buffer != null);
        w.hexpand = true;
        w.set_wrap_mode (Gtk.WrapMode.WORD);
        w.set_auto_indent (true);
        w.set_indent_on_tab (true);
        win.waiting_for_event = TestConfig.INTERACTIVE;
        w.server = server;
        win.widget = w;
        win.add_test ("Parse pre-defined buffer with fields", "A text on view should be parsed. Try to use a symbol");
        win.initialize.connect (()=>{
          try {
            w.server.add_default_vapi_dirs ();
            Server server2 = new GServer () as Server;
            server2.parse (file);
            assert (server2.get_symbol ("MyOwn") != null);
            assert (server2.get_symbol ("MyOwn.App") != null);
            w.server.add_server (server2);
            message ("Servers: %u", w.server.servers.get_n_items ());
            assert (w.server.servers.get_n_items () == 2);
            for (int i = 0; i < w.server.servers.get_n_items (); i++) {
              var s = w.server.servers.get_item (i) as GVls.Server;
              message (s.file.get_uri ());
            }
            assert (w.server.get_symbol ("MyOwn") != null);
            assert (w.server.get_symbol ("MyOwn.App") != null);
            message ("Current Servers: %u", server.servers.get_n_items ());
          } catch (GLib.Error e) {
            message ("Error adding defualt VAPI dirs: %s", e.message);
            assert_not_reached ();
          }
          message ("Setting Text to Buffer");
          w.buffer.text = """
using GLib;

public class Test {
  private string _sound;
  public void method () {}
  public string name { get; set; }
  public class SubTest : Object {
    public struct Dot {
      public int x;
      public int y;
    }
    public enum Sizes {
      BIG,
      SMALL
    }
  }
}
public interface IDTest : Object {
  public abstract SubTest ts { get; }
  public abstract Test parent ();
  public class DTest : Object, IDTest {
    private SubTest _ts;
    public SubTest ts { get { return _ts; } }
    public Test parent () {
      return new Test ();
    }
  }
}
public struct Data {
  public int hour;
  public int day;
}
pubic enum Values {
  ONE, TWO, THREE
}
public class App {
  public static void main () {
    Test t;
    Data d;
    Values v;
  }
}

""";
          assert (server.get_symbol ("GLib") != null);
          message ("Current Servers: %u", server.servers.get_n_items ());
          assert (server.servers.get_n_items () == 2);
        });
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    return Test.run ();
  }
}
