/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvdaui Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GVlsui;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Gtk.init (ref args);
    Test.add_func ("/gvlsui/sourceview/format/symbols",
    ()=>{
      Server server = new GServer () as Server;
      try {
        var win = new Gtkt.WindowTester ();
        var w = new GVlsui.SourceView ();
        assert (w.buffer != null);
        w.hexpand = true;
        w.set_wrap_mode (Gtk.WrapMode.WORD);
        w.set_auto_indent (true);
        w.set_indent_on_tab (true);
        win.waiting_for_event = TestConfig.INTERACTIVE;
        w.server = server;
        win.widget = w;
        win.add_test ("Syntax highligth", "keywords/types");
        win.initialize.connect (()=>{
          message ("Setting Text to Buffer");
          w.buffer.text = """
public class Test {}
""";
        });
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    return Test.run ();
  }
}
