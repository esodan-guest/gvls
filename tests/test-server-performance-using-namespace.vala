/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GVls Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gvls/performance/using-namespace",
    ()=>{
      var server = new GServer ();
      try {
        Test.timer_start ();
        for (int i = 0; i < 100; i++) {
          message ("Trying to add GLib ns: %d", i);
          server.add_namespace ("GLib");
        }
        var t1 = Test.timer_elapsed ();
        message ("In seconds: t1: %s;", t1.to_string ());
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    return Test.run ();
  }
}
