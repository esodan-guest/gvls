/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class Client : GLib.Object, GVls.Client {
  public bool parse_mode { get; internal set; }
  public string str { get; internal set; }
  public Server server { get; construct set; }
  public Client (Server s) {
    server = s;
  }
}

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gvls/client/symbol-type",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var client = new Client (server);
        assert (client.symbol_kind ("take_some") == SymbolKind.UNKNOWN);
        assert (client.symbol_kind ("App") == SymbolKind.CLASS);
        assert (client.symbol_kind ("App2") == SymbolKind.CLASS);
        assert (client.symbol_kind ("IfaceApp") == SymbolKind.INTERFACE);
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/client/push",
    ()=>{
      GLib.MainLoop loop = new GLib.MainLoop ();
      Server server = new GServer () as Server;
      var client = new Client (server);
      Idle.add (()=>{
        try  {
          client.push_text ("class Push {\n");
          assert (server.get_symbol ("Push") != null);
          client.push_text ("\tpublic void callme () {");
          assert (server.get_symbol ("Push.callme") != null);
          client.push_text ("\n\t}");
          assert (server.get_symbol ("Push.callme") != null);
          client.push_text ("\n}\n");
          assert (server.get_symbol ("Push.callme") != null);
          client.push_text ("class SubPush {\n");
          assert (server.get_symbol ("SubPush") != null);
          message ("Parsed String: \n%s", client.str);
        } catch (GLib.Error e) {
          warning ("Error while in parse mode: %s", e.message);
        }
        loop.quit ();
        return GLib.Source.REMOVE;
      });
      loop.run ();
    });
    Test.add_func ("/gvls/client/completion/number-items",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file.query_exists ());
      GLib.MainLoop loop = new GLib.MainLoop ();
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var client = new Client (server);
        client.completion.connect ((list)=> {
          message ("Completion items: %u", list.get_n_items ());
          for (int i = 0; i < list.get_n_items (); i++) {
            message ("Item%d: %s", i, (list.get_item (i) as Symbol).name);
          }
          assert (list.get_n_items () == 7);
          loop.quit ();
        });
        Idle.add (()=>{
          try {
            client.push_text ("App.");
           } catch (GLib.Error e) {
            warning ("Error while completion: %s", e.message);
           }
          loop.quit ();
          return GLib.Source.REMOVE;
        });
        loop.run ();
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/client/completion/dotted",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file.query_exists ());
      GLib.MainLoop loop = new GLib.MainLoop ();
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        var client = new Client (server);
        client.completion.connect ((list)=> {
          message ("Enum: Completion items: %u", list.get_n_items ());
          for (int i = 0; i < list.get_n_items (); i++) {
            message ("Item%d: %s", i, (list.get_item (i) as Symbol).name);
          }
          assert (list.get_n_items () == 2);
          loop.quit ();
        });
        Idle.add (()=>{
          try {
            client.push_text ("MyOwn.SubMyOwn.Task.SubTask.Tasks.");
           } catch (GLib.Error e) {
            warning ("Error while completion: %s", e.message);
           }
          loop.quit ();
          return GLib.Source.REMOVE;
        });
        loop.run ();
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    Test.add_func ("/gvls/client/completion/variable",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file.query_exists ());
      GLib.MainLoop loop = new GLib.MainLoop ();
      Server server = new GServer () as Server;
      Server server2 = new GServer () as Server;
      try {
        server2.parse (file);
        server.add_server (server2);
        var client = new Client (server);
        client.completion.connect ((list)=> {
          message (client.str);
          Symbol sym = list as Symbol;
          message ("Symbol: %s", sym.name);
          message ("Symbol's type: %s", sym.data_type);
          message ("Symbol's kind: %s", sym.symbol_kind.to_string ());
          message ("Symbol: Completion items: %u", list.get_n_items ());
          for (int i = 0; i < list.get_n_items (); i++) {
            message ("Item%d: %s", i, (list.get_item (i) as Symbol).name);
          }
          if (sym.name == "t") {
            assert (list.get_n_items () == 2);
            loop.quit ();
          }
        });
        Idle.add (()=>{
          try {
            client.push_text ("""
using MyOwn;

public class Two : Object {
  public void method () {}
  public string name { get; set; }
  public class SubM : Object {
    public string to_string() { return ""; }
    public enum EnumC {
      ONE, TWO, THREE
    }
  }
}
public class App {
  public static void main () {
    Two t;
  }
}""");
            assert (server.get_symbol ("main") != null);
            assert (server.get_symbol ("t") != null);
            for (int k = 0; k < server.symbols.get_n_items (); k++) {
              Symbol s = server.symbols.get_item (k) as Symbol;
              message ("Root symbol: %s", s.name);
            }
            message ("pushing test to rise event...");
            // FIXME: See issue #21
            //client.push_text ("t.");
          } catch (GLib.Error e) {
          warning ("Error while completion: %s", e.message);
          }
          loop.quit ();
          return GLib.Source.REMOVE;
        });
        loop.run ();
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    return Test.run ();
  }
  public static int check_kind (MainLoop loop, string str, SymbolKind kind) {
    message ("Checking word kind: %s : %s", str, kind.to_string ());
    int res = 0;
    switch (str) {
      case "App":
        assert (kind == SymbolKind.CLASS);
        res = 1;
        break;
      case "App2":
        assert (kind == SymbolKind.CLASS);
        res = 1;
        break;
      case "IfaceApp":
        assert (kind == SymbolKind.INTERFACE);
        res = 1;
        break;
    }
    return res;
  }
}
