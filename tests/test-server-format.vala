/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvda Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gvls/format/document-symbols",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      assert (file.query_exists ());
      Server server = new GServer () as Server;
      try {
        server.parse (file);
        message ("Symbols Found: %u", server.symbols.get_n_items ());
        message ("Keywords Found: %u", server.keywords.get_n_items ());
        assert (server.keywords.get_n_items () > 0);
        var sd = server.document_symbols;
        assert (sd.get_n_items () != 0);
        message ("DocumentSymbol Found: %u", sd.get_n_items ());
        for (int i = 0; i < sd.get_n_items (); i++) {
          var sym = sd.get_item (i) as DocumentSymbol;
          assert (sym != null);
          message ("DocumentSymbol found: %s", sym.name);
        }
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    return Test.run ();
  }
}
