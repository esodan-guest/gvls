using Cairo;
/**
 *
 */
public class App {
  public string name { get; set; }
  public void method (string val) {}
}
/**
 * Test documentation one line
 */
public class App2 {
  /**
   * Test documentation with
   * multiple lines in the
   * help text
   */
  public void callme () {}
}

/**
 * Test interface with documentation
 * in place, multiple paragrap.
 *
 * Second paragraph.
 */
public interface IfaceApp {
  public abstract void callme () {}
}

public class Main : Object {
  public static void main () {
    for (int i = 0; i < 0; i++) {
      stdout.printf ("Val: %d", i);
    }
  }
}
