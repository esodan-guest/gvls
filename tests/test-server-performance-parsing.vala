/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GVls Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gvls/performance/parsing",
    ()=>{
      var envp = GLib.Environ.get ();
      string SRC_DIR = GLib.Environ.get_variable (envp, "SRC_DIR");
      GLib.File file1 = GLib.File.new_for_path (SRC_DIR+"/parse.vala");
      GLib.File file2 = GLib.File.new_for_path (SRC_DIR+"/namespace.vala");
      assert (file1.query_exists ());
      assert (file2.query_exists ());
      var server = new GServer ();
      var server2 = new GServer ();
      var server3 = new GServer ();
      try {
        Test.timer_start ();
        server2.parse (file1);
        server.add_server (server2);
        var t1 = Test.timer_elapsed ();
        server3.parse (file2);
        var t2 = Test.timer_elapsed ();
        server.add_server (server3);
        var t3 = Test.timer_elapsed ();
        server.add_pkg ("gobject-2.0");
        var t4 = Test.timer_elapsed ();
        server.add_pkg ("gio-2.0");
        var t5 = Test.timer_elapsed ();
        var srv = new GServer ();
        server.add_server (srv);
        string t = "using GLib;";
        for (int i = 0; i < 100; i++) {
          t += """
public class Test%d : Object {
  public string name { get; set; }
  public void method (int num) {
    int sti;
  }
}
""".printf (i);
        }
        srv.content = t;
        var t6 = Test.timer_elapsed ();
        message ("In seconts: t1: %s; t2: %s; t3: %s; t4: %s; t5: %s; t6: %s",
                t1.to_string (), t2.to_string (), t3.to_string (),
                t4.to_string (), t5.to_string (), t6.to_string ());
      } catch (GLib.Error e) {
        message ("Error: %s", e.message);
        assert_not_reached ();
      }
    });
    return Test.run ();
  }
}
