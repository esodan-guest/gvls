Source: gvls
Priority: optional
Maintainer: Daniel Espinosa <esodan@gmail.com>
Build-Depends: debhelper (>= 11~), valac, meson (>= 0.40.0), ninja-build, pkg-config, libglib2.0-dev, libgee-0.8-dev, libgtk-3-dev, libgtksourceview-3.0-dev, libvala-0.40-dev, valadoc, libvaladoc-0.40-dev, libjsonrpc-glib-1.0-dev, gobject-introspection, libgirepository1.0-dev, gedit-dev, gedit-developer-plugins
Standards-Version: 4.1.2
Section: libs
Homepage: https://gitlab.gnome.org/esodan/gvls/wikis/home
Vcs-Git: https://gitlab.gnome.org/esodan/gvls.git
Vcs-Browser: https://gitlab.gnome.org/esodan/gvls

Package: libgvls-0.10
Section: libs
Architecture: any
Multi-Arch: same
Depends: libglib2.0-0, libgee-0.8-2, libvala-0.40-0, libvaladoc-0.40-0, , libjsonrpc-glib-1.0-1, ${misc:Depends}, ${shlibs:Depends}
Description: GNOME Vala Language Server library
 Provides a set of objects useful to parse, navigate and
 provide syntax highligth, among others.
 .
 Currently it provides:
 * Tree of symbol servers, one per file to parse
 * Automatic loading of VAPI files referenced by using directive
 * Multi-file parse for symbol search
 * Document Symbol map useful for syntax highligh

Package: libgvls-0.10-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libglib2.0-dev, libgee-0.8-dev, libvala-0.40-dev, libgvls-0.10 (= ${binary:Version}), libvaladoc-0.40-dev, , libjsonrpc-glib-1.0-dev, ${misc:Depends}
Description: GNOME Vala Language Server library
 Provides a set of objects useful to parse, navigate and
 provide syntax highligth, among others

Package: libgvlsui-0.10
Section: libs
Architecture: any
Multi-Arch: same
Depends: libgvls-0.10 (>= 0.10.0), libgtk-3-0, libgtksourceview-3.0-1, ${misc:Depends}, ${shlibs:Depends}
Description: GNOME Vala Language Server library
 Provides a set of objects useful to parse, navigate and
 provide syntax highligth, among others.
 .
 Currently provides:
 * A custom GtkSourceView widget providing symbol autocompletion and syntax highligth

Package: libgvlsui-0.10-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libgtk-3-dev, libgtksourceview-3.0-dev, libvala-0.40-dev, libgvls-0.10-dev, libgvlsui-0.10 (= ${binary:Version}), ${misc:Depends}
Description: GNOME Vala Language Server library providing GTK+ widgets devel
 Development files for GVls GKT+ widgets.

Package: libgvlsui-0.10-gedit-plugin
Section: gnome
Architecture: any
Multi-Arch: same
Depends: libgvls-0.10 (>= 0.10.0), libgvlsui-0.10 (>= 0.10.0), libpeas-1.0, gedit, ${misc:Depends}, ${shlibs:Depends}
Description: Plugin for GEdit using GVls
 Provides a GEdit plugin implementtion providing Vala symbol completion
 and Vala syntax highligthing

Package: libgvls-0.10-doc
Section: doc
Architecture: any
Multi-Arch: same
Depends: libgvls-0.10 (>= 0.10.0), valadoc, ${misc:Depends}, ${shlibs:Depends}
Description: GNOME Vala Language Server Library documentation
 Provides DevHelp documentation for GVls

Package: gir1.2-gvls-0.10
Section: introspection
Architecture: any
Multi-Arch: same
Depends: libgvls-0.10 (>= 0.10.0), libgirepository-1.0-1, ${misc:Depends}, ${shlibs:Depends}, ${gir:Depends}
Description: GNOME Vala Language Server Library GObject Introspection files
 This package contains introspection data for GVls
